<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use App\Http\Requests;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Config;
use View;
use Illuminate\Support\Collection;

use GuzzleHttp\Client;
use GuzzleHttp\Promise as GuzzlePromise;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;

// database access
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class CronJobController extends Controller
{

    /**
     * CronJob Execute Callback Function
     */

    public function addCycles(){

    	$date_year = 2037;

		for($i=2019; $i<=$date_year; $i++){
			$month = 12;
			for($j = 1; $j<=12; $j++){
				
				if($j < date('m')-1 AND $i <= 2019 ) {
					continue;
				}

				$zero = ($j <= 9) ? '0' : '';
				
				$mon = ($j == 12) ? 1 : $j+1;

				$yr = ($j == 12) ? $i+1 : $i;

				// echo ($j==12) ? "this is 12th<hr>" : " else <hr>";
				// echo $yr."<br>";

				$startDate = $i."/".$j."/11 00:00:00";

				$endDate = $yr."/".$mon."/10 23:59:59";


				$date_start = strtotime($startDate);
				$data_start_conv = date('Y-m-d H:i:s', $date_start);

				$date_end = strtotime($endDate);
				$data_end_conv = date('Y-m-d H:i:s', $date_end);

				// echo $startDate." _____ ".$endDate."<br>";
				// echo $data_start_conv." _____ ".$data_end_conv."<hr>";


				$cycles = DB::table('cycle')->where('start_date', '>=', $data_start_conv)->where('end_date', '<=', $data_end_conv);

				if ($cycles->count() > 0) {
					continue;

					// echo "<pre>";
					// print_r($cycles->get());
					// echo "</pre>";

				} else {
					$cycles =  DB::table('cycle')->insert(['start_date' => $data_start_conv, 'end_date' => $data_end_conv]);  // result "true"
					// echo "<pre>";
					// print_r($cycles->get());
					// echo "</pre>";
				}
				
				
			}
		}
    }

    /**
     * Execute Call back function for CronJob
     *  
     */

    public function cronJobExecute(){

    	
         //
        $client = new Client();
        /*-------------- GET -> All Numbers  ---------------*/

        $resutCount = 0;
        $itterationFailed = 0;
        // mno=att, mno=tmo
        $response = $client->request('GET', config('app.goknowsAPI.api_url').'lines', [
            'headers' => [
                'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),                
            ]
        ]);
        $result= $response->getBody();
        $phone_data = json_decode($result);

        // print_r($phone_data);
        // die();

        $cron_run = false;
        $ban_number_new = array();

        if($phone_data) {

            foreach ($phone_data as $key => $value) {
                // if($resutCount == 10){
                //  break;
                // }


                $phone_number =  $value->phone_number;

                // $ban_number =  $value->ban;

                // echo $ban_number.'\n\n';


                // if($ban_number !="961131459") {
                //  continue;
                // }

                // $cron_log_res = DB::table('cron_log')->order_by('id', 'desc')->first();
                $cron_log_res = DB::table('cron_log')->orderBy('id', 'DESC')->first();

                if (!empty($cron_log_res) ) {
                    $cron_last_log_date = $cron_log_res->run_date;
                } else {
                    // Add Record in CronLog table if not found any
                    $run_date = date('Y-m-d H:i:s');
                    $cron_data = array( 'run_date' => $run_date, 'success' => 1 );
                    DB::table('cron_log')->insert($cron_data);  // result "true"
                    $cron_log_id = DB::getPdo()->lastInsertId();
                    $cron_log_res = DB::table('cron_log')->where('id', '=', $cron_log_id);
                    $res = $cron_log_res->get()->toArray();
                    $cron_last_log_date = $res[0]->run_date;
                }

                // check Cycle Entry from cycle table

                $cycles = DB::table('cycle')->where('start_date', '<=', $cron_last_log_date)->where('end_date', '>=', $cron_last_log_date);

                if ($cycles->count() > 0) {
                    $res = $cycles->get()->toArray();
                    $cycle_id = $res[0]->id;
                    $cycle_start_date = date('Y-m-d',strtotime($res[0]->start_date));
                    $cycle_end_date = date('Y-m-d',strtotime($res[0]->end_date));

                }

                $responseUsage = $client->request('GET', config('app.goknowsAPI.api_url').'lines/'.$phone_number.'/usage_details', [
                    'headers' => [
                        'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                    ]
                    // ,
                    // 'form_params' => [
              //           'from_date' => $cycle_start_date, 
              //           'until_date' => $cycle_end_date 
              //       ]
                ]);



                $resultUsage= $responseUsage->getBody();
                $phone_data_Usage = json_decode($resultUsage);




                // print_r($phone_data_Usage);
                

                // Ban table Record
                // echo " CYCLE_ID: ".$cycle_id ." __ ".$phone_number;
                // die();

                // $phone_data_Usage = get_object_vars($phone_data_Usage);

                

                if($phone_data_Usage != NULL){
                    $phone_data_Usage = get_object_vars($phone_data_Usage);
                } else {
                    $phone_data_Usage = '';
                }


                if(isset($phone_data_Usage) AND !empty($phone_data_Usage)) {

                    if(isset($phone_data_Usage['error'])){

                        Log::info('Showing user profile for user: '. $phone_data_Usage['error']);
                        

                        $itterationFailed ++;

                        $error = $phone_data_Usage['error'];
                        $title = $phone_data_Usage['title'];
                        $message = $phone_data_Usage['message'];
                        $code = $phone_data_Usage['code'];
                        $status = $phone_data_Usage['status'];

                        // echo "Error: ".$error." \nTitle:" .$title." \nMessage: ".$message." \nCode:". $code."\n\n\n";
                        continue;
                    } else {

                        $ban_number_new[] =  $phone_data_Usage['financialAccountNumber'];


                        $ban_number = $phone_data_Usage['financialAccountNumber'];

                        
                        $ban_res = DB::table('ban')->where('cycle_id', '=', $cycle_id)->where('number', '=', $ban_number);
                        if ($ban_res->count() == 0) {

                            $ban_data = array(
                                'cycle_id' => $cycle_id,
                                'number' => $ban_number
                            );

                            DB::table('ban')->insert($ban_data);  // result "true"
                            $ban_id = DB::getPdo()->lastInsertId();
                            // dd($id);
                            // echo " INSERTED Ban ID > ".  $ban_id."<br>";

                        } else {
                            $ban_row = $ban_res->get()->toArray();
                            $ban_id = $ban_row[0]->id;
                            // echo " FETCHED Ban ID > ".   $ban_id."<br>";
                        }

                        $phone_number_res = DB::table('phone_number')->where('cycle_id', '=', $cycle_id)->where('number', '=', $phone_number);


                        // $phone_number_res = DB::table('phone_number')->where('cycle_id', '=', $cycle_id)->where('ban_id', '=', $ban_id)->where('number', '=', $phone_number);
                        if ($phone_number_res->count() == 0) {
                            // echo "PHONE NUMBER ADDED!";

                            $phone_number_data = array(
                                'cycle_id' => $cycle_id,
                                'number' => $phone_number,
                                'ban_id' => $ban_id
                            );

                            DB::table('phone_number')->insert($phone_number_data);  // result "true"
                            $phone_number_id = DB::getPdo()->lastInsertId();
                            // dd($id);
                        }else {
                            // echo "PHONE ALREADY EXIST!";
                            $phone_number_row = $phone_number_res->get()->toArray();
                            // print_r($phone_number_row);
                            $phone_number_id = $phone_number_row[0]->id;
                        }


                        $data_log_ratedAmount = 0;
                        $data_log_callVolume = 0;
                        $phone_number_texts = 0;
                        $text_log_ratedAmount = 0;
                        $voice_log_ratedAmount = 0;
                        $voice_log_num = 0;
                        $voice_log_callDuration =0;


                        // print_r($phone_data_Usage['data']);
                        foreach ($phone_data_Usage['data'] as $key => $valueUsage) 
                        {
                            
                            // echo "<h1>DATA</h1>";

                            $channelSeizureDate = $valueUsage->channelSeizureDate;
                            $callType = $valueUsage->callType;
                            $bucket = $valueUsage->bucket;
                            if(!empty($bucket)) {
                                $bucketService = $bucket[0]->bucketService;
                                $bucketFeature = $bucket[0]->bucketFeature;
                            } else {
                                $bucketService = '';
                                $bucketFeature = '';
                            }
                            $ratedAmount = $valueUsage->ratedAmount;
                            $callVolume = $valueUsage->callVolume;
                            $messageType = $valueUsage->messageType;
                            $roamingIndicator = $valueUsage->roamingIndicator;
                            $unitOfMeasure = $valueUsage->unitOfMeasure;
                            $usageCategory = $valueUsage->usageCategory;

                            $data_log = array(
                                'phone_number_id' => $phone_number_id,
                                'channelSeizureDate' => $channelSeizureDate,
                                'callType' => $callType,
                                'bucketService' => $bucketService,
                                'bucketFeature' => $bucketFeature,
                                'ratedAmount' => $ratedAmount,
                                'callVolume' => $callVolume,
                                'messageType' => $messageType,
                                'roamingIndicator' => $roamingIndicator,
                                'unitOfMeasure' => $unitOfMeasure,
                                'usageCategory' => $usageCategory,
                                'created_at' => date('Y-m-d H:i:s')
                            );

                            // DB::table('data_log')->insert($data_log);  // result "true"

                            $data_log_chck = DB::table('data_log')->where('channelSeizureDate', '=', $channelSeizureDate)->where('phone_number_id', '=', $phone_number_id);

                            if ($data_log_chck->count() > 0) {
                                continue;

                                // echo "<pre>";
                             //     print_r($data_log_chck->get());
                                // echo "</pre>";

                            } else {
                                $data_log_res =  DB::table('data_log')->insert($data_log);  // result "true"
                            }

                        }
                        foreach ($phone_data_Usage['text'] as $key => $valueUsage) 
                        {
                            
                            // echo "<h1>TEXT</h1>";


                            $callType = $valueUsage->callType;
                            $channelSeizureDate = $valueUsage->channelSeizureDate;
                            $destinationCity = $valueUsage->destinationCity;
                            $destinationNumber = $valueUsage->destinationNumber;
                            $destinationState = $valueUsage->destinationState;
                            $direction = $valueUsage->direction;
                            $bucket = $valueUsage->bucket;
                            if(!empty($bucket)) {
                                $bucketService = $bucket[0]->bucketService;
                                $bucketFeature = $bucket[0]->bucketFeature;
                            } else {
                                $bucketService = '';
                                $bucketFeature = '';
                            }
                            $ratedAmount = $valueUsage->ratedAmount;
                            $messageType = $valueUsage->messageType;
                            $billLegend = $valueUsage->billLegend;
                            $origin = $valueUsage->origin;
                            $roamingIndicator = $valueUsage->roamingIndicator;
                            $unitOfMeasure = $valueUsage->unitOfMeasure;
                            $usageCategory = $valueUsage->usageCategory;

                            $text_log = array(
                                'callType' => $callType,
                                'phone_number_id' => $phone_number_id,
                                'channelSeizureDate' => $channelSeizureDate,
                                'destinationCity' => $destinationCity,
                                'destinationNumber' => $destinationNumber,
                                'destinationState' => $destinationState,
                                'direction' => $direction,
                                'bucketService' => $bucketService,
                                'bucketFeature' => $bucketFeature,
                                'ratedAmount' => $ratedAmount,
                                'messageType' => $messageType,
                                'billLegend' => $billLegend,
                                'origin' => $origin,
                                'roamingIndicator' => $roamingIndicator,
                                'unitOfMeasure' => $unitOfMeasure,
                                'usageCategory' => $usageCategory,
                                'created_at' => date('Y-m-d H:i:s')
                            );

                            // DB::table('text_log')->insert($text_log);  // result "true"

                            $text_log_chck = DB::table('text_log')->where('channelSeizureDate', '=', $channelSeizureDate)->where('destinationNumber','=', $destinationNumber)->where('phone_number_id', '=', $phone_number_id);

                            if ($text_log_chck->count() > 0) {
                                // continue;
                            } else {
                                $text_log_res =  DB::table('text_log')->insert($text_log);  // result "true"
                            }

                        }

                        foreach ($phone_data_Usage['voice'] as $key => $valueUsage) 
                        {

                            // echo "<h1>VOICE</h1>";

                            $callDuration = $valueUsage->callDuration;
                            $callType = $valueUsage->callType;
                            $carrier = $valueUsage->carrier;
                            $channelSeizureDate = $valueUsage->channelSeizureDate;
                            $destinationCity = $valueUsage->destinationCity;
                            $destinationNumber = $valueUsage->destinationNumber;
                            $destinationState = $valueUsage->destinationState;
                            $originCity = $valueUsage->originCity;
                            $originState = $valueUsage->originState;
                            $bucket = $valueUsage->bucket;
                            if(!empty($bucket)) {
                                $bucketService = $bucket[0]->bucketService;
                                $bucketFeature = $bucket[0]->bucketFeature;
                            } else {
                                $bucketService = '';
                                $bucketFeature = '';
                            }
                            $ratedAmount = $valueUsage->ratedAmount;
                            $billLegend = $valueUsage->billLegend;
                            $messageType = $valueUsage->messageType;
                            $direction = $valueUsage->direction;
                            $origin = $valueUsage->origin;
                            $roamingIndicator = $valueUsage->roamingIndicator;
                            $unitOfMeasure = $valueUsage->unitOfMeasure;
                            $usageCategory = $valueUsage->usageCategory;

                            $voice_log = array(
                                'callDuration' => $callDuration,
                                'phone_number_id' => $phone_number_id,
                                'callType' => $callType,
                                'carrier' => $carrier,
                                'channelSeizureDate' => $channelSeizureDate,
                                'destinationCity' => $destinationCity,
                                'destinationNumber' => $destinationNumber,
                                'destinationState' => $destinationState,
                                'originCity' => $originCity,
                                'originState' => $originState,
                                'bucketService' => $bucketService,
                                'bucketFeature' => $bucketFeature,
                                'ratedAmount' => $ratedAmount,
                                'billLegend' => $billLegend,
                                'messageType' => $messageType,
                                'direction' => $direction,
                                'origin' => $origin,
                                'roamingIndicator' => $roamingIndicator,
                                'unitOfMeasure' => $unitOfMeasure,
                                'usageCategory' => $usageCategory,
                                'created_at' => date('Y-m-d H:i:s')
                            );

                            $voice_log_chck = DB::table('voice_log')->where('channelSeizureDate', '=', $channelSeizureDate)->where('destinationNumber','=', $destinationNumber)->where('phone_number_id', '=', $phone_number_id);

                            if ($voice_log_chck->count() > 0) {
                                // continue;
                            } else {
                                $voice_log_res =  DB::table('voice_log')->insert($voice_log);  // result "true"
                            }
                        }
                    }

                    // Update data_log.ratedAmount and data_log.callVolume in tables
                    $data_log_call_volume = DB::table('data_log')
                    ->select(
                        DB::raw('SUM(callVolume) as data_log_callVolume'), 
                        DB::raw('SUM(ratedAmount) as data_log_ratedAmount')
                    )
                    ->where('phone_number_id', $phone_number_id)
                    ->get()
                    ->toArray();

                    $data_log_ratedAmount = $data_log_call_volume[0]->data_log_ratedAmount;
                    $data_log_callVolume = $data_log_call_volume[0]->data_log_callVolume;

                    // $data_log_callVolume = $data_log_call_volume[0]->data_log_callVolume;

                    $text_log_call_volume = DB::table('text_log')
                    ->select(
                        DB::raw('COUNT(*) as phone_number_texts'), 
                        DB::raw('SUM(ratedAmount) as text_log_ratedAmount')
                    )
                    ->where('phone_number_id', $phone_number_id)
                    ->get()
                    ->toArray();

                    $phone_number_texts = $text_log_call_volume[0]->phone_number_texts;
                    $text_log_ratedAmount = $text_log_call_volume[0]->text_log_ratedAmount;

                    // $phone_number_texts = $text_log_call_volume[0]->phone_number_texts;

                    $voice_log_call_volume = DB::table('voice_log')
                    ->select(
                        DB::raw('SUM(ratedAmount) as voice_log_ratedAmount'), 
                        DB::raw('COUNT(*) as voice_log_num'), 
                        DB::raw('SUM(callDuration) as voice_log_callDuration')
                    )
                    ->where('phone_number_id', $phone_number_id)
                    ->get()
                    ->toArray();

                    $voice_log_ratedAmount = $voice_log_call_volume[0]->voice_log_ratedAmount;
                    $voice_log_num = $voice_log_call_volume[0]->voice_log_num;
                    $voice_log_callDuration =$voice_log_call_volume[0]->voice_log_callDuration;

                    // $voice_log_callVolume = $voice_log_call_volume[0]->voice_log_callVolume;


                    $phone_update_arr = array(
                        'data_rated_amount' => $data_log_ratedAmount,
                        'data' => $data_log_callVolume,
                        'text_rated_amount' => $text_log_ratedAmount,
                        'texts' => $phone_number_texts,
                        'voice_rated_amount' => $voice_log_ratedAmount,
                        'voice_min' => $voice_log_callDuration,
                        'voice_num' => $voice_log_num
                    );
                    DB::table('phone_number')->where('number', $phone_number)->update($phone_update_arr);

                    // Get Updated Data from particular phone number and update in Ban Table

                    $ban_phone_number_data = DB::table('phone_number')
                    ->select(
                        DB::raw('SUM(data_rated_amount) AS data_rated_amount'),
                        DB::raw('SUM(data) AS data'),
                        DB::raw('SUM(texts) AS texts'),
                        DB::raw('SUM(text_rated_amount) AS text_rated_amount'),
                        DB::raw('SUM(voice_rated_amount) AS voice_rated_amount'),
                        DB::raw('SUM(voice_min) AS voice_min'),
                        DB::raw('SUM(voice_num) AS voice_num')
                    )
                    ->where('ban_id', $ban_id)
                    ->get()
                    ->toArray();

                    // update `ban` table
                    $ban_update_arr = array(
                        'data_rated_amount' => $ban_phone_number_data[0]->data_rated_amount,
                        'data' => $ban_phone_number_data[0]->data,
                        'text_rated_amount' => $ban_phone_number_data[0]->text_rated_amount,
                        'texts' => $ban_phone_number_data[0]->texts,
                        'voice_rated_amount' => $ban_phone_number_data[0]->voice_rated_amount,
                        'voice_min' => $ban_phone_number_data[0]->voice_min,
                        'voice_num' => $ban_phone_number_data[0]->voice_num
                    );

                    DB::table('ban')->where('id', $ban_id)->update($ban_update_arr);

                    // echo "Phone Number: ".$phone_number." Ban: ".$ban_number."\n";

                    $resutCount++;

                    $cron_run = true;
                } else {
                    continue;
                }

            }
        }

        // Add Record in CronJob log Table `cron_log` table

        $run_date = date('Y-m-d H:i:s');

        if($cron_run == true) {
            $cron_data = array( 'run_date' => $run_date, 'success' => 1 );
        } else {
            $cron_data = array('run_date' => $run_date, 'success' => 0, 'error' => 'Cronjob Not run it cause some error!');
        }

        DB::table('cron_log')->insert($cron_data);  // result "true"

        echo "Failed itterations: ".$itterationFailed."\n\n";
        echo "resutCount: ".$resutCount."\n\n";

        print_r($ban_number_new);

    }

    /**
     * Get Latest Ban Records
     *  
     */

    public function getBanLatestLines(Request $request)
    {
    	$this->checkLoggedin($request);
    	// dd($data);

    	$data = array();
		$billing_cycle_list = DB::table('cycle')->get()->toArray();
    	$data['billing_cycle_list'] = $billing_cycle_list;

    	$currentdate = date('Y-m-d H:i:s');

    	$cycles = DB::table('cycle')->where('start_date', '<=', $currentdate)->where('end_date', '>=', $currentdate);

		if ($cycles->count() > 0) {
		   	$res = $cycles->get()->toArray();
		   	$cycle_id = $res[0]->id;
		}


    	$cycles_data = DB::table('ban')->where('cycle_id', '=', $cycle_id)->limit(5);

    	if ($cycles_data->count() > 0) {
    		$data['result_ban_line'] = $cycles_data->get()->toArray();
    	} else {
    		$data['result_ban_line'] = array();
    	}

    	return View::make('pages.banpage',$data);

    }

    /**
     * Get Ban Records by cycle ID
     *  
     */


    public function getBanCycleLines(Request $request,$cycle_id=0)
    {
    	$this->checkLoggedin($request);
    	// dd($data);

    	$data = array();
		$billing_cycle_list = DB::table('cycle')->get()->toArray();
    	$data['billing_cycle_list'] = $billing_cycle_list;

    	$cycles_data = DB::table('ban')->where('cycle_id', '=', $cycle_id);
    	
    	if ($cycles_data->count() > 0) {
    		$data['result_ban_line'] = $cycles_data->get()->toArray();
    	} else {
    		$data['result_ban_line'] = array();
    	}

    	return View::make('pages.banpage',$data);

    }

    /**
     * Get Ban Detail by Ban ID
     *  
     */


	public function getBanDetail(Request $request,$ban_id=0)
    {
    	$this->checkLoggedin($request);

		$data = array();

		$billing_cycle_list = DB::table('cycle')->get()->toArray();
    	$data['billing_cycle_list'] = $billing_cycle_list;

    	$phone_number_res = DB::table('phone_number')->where('ban_id', '=', $ban_id);
		
		$banRes = DB::table('ban')->where('id', '=', $ban_id)->first();
		if (!empty($banRes) ) {
			$data['ban_number'] =  $banRes->number;
		} 
		$phone_number_res->get()->toArray();

		$data_rated_amount_sum = 0;
		$data_sum = 0;
		$texts_sum = 0;
		$voice_min_sum = 0;
		$voice_rated_amount_sum = 0;
		$text_rated_amount_sum = 0;
		$voice_num_sum = 0;


		if ($phone_number_res->count() > 0) {
		   	$phone_numbers_res_data = $phone_number_res->get()->toArray();

		   	foreach ($phone_numbers_res_data as $value) {
				$data_rated_amount_sum += $value->data_rated_amount;
				$data_sum += $value->data;
				$texts_sum += $value->texts;
				$voice_min_sum += $value->voice_min;

				$voice_rated_amount_sum += $value->voice_rated_amount;
				$text_rated_amount_sum += $value->text_rated_amount;
				$voice_num_sum += $value->voice_num;
		   	}

		   	$data['data_rated_amount_sum'] = $data_rated_amount_sum;
			$data['data_sum'] = $data_sum;
			$data['texts_sum'] = $texts_sum;
			$data['voice_min_sum'] = $voice_min_sum;
			$data['voice_rated_amount_sum'] = $value->voice_rated_amount;
			$data['text_rated_amount_sum'] = $value->text_rated_amount;
			$data['voice_num_sum'] = $value->voice_num;

		   	$data['phone_numbers_list'] = $phone_numbers_res_data;

		} else {
			$data['phone_numbers_list'] = array();
		}

    	return View::make('pages.phonenumberslist',$data);

    }

    /**
     * Get Phone Detail with All tables(data_log, text_log, voice_log ) Detail by phone_number_id ID
     *  
     */

    public function phoneDetail(Request $request,$phone_number_id =0 )
    {
    	$this->checkLoggedin($request);

    	$data = array();

    	$billing_cycle_list = DB::table('cycle')->get()->toArray();
    	$data['billing_cycle_list'] = $billing_cycle_list;

    	$voice_log_num =0;
		$voice_log_ratedAmount = 0;
		$voice_log_callDuration = 0;
		$text_log_ratedAmount = 0;
		$phone_number_texts =0;
		$data_log_ratedAmount = 0;
		$data_log_callVolume = 0;

    	$phone_number_res = DB::table('phone_number')->where('id', '=', $phone_number_id);
		$phone_number_res->get()->toArray();

		if ($phone_number_res->count() > 0) {
		   	$phone_numbers_res_data = $phone_number_res->get()->toArray();
		   	$data['phone_numbers_list'] = $phone_numbers_res_data;
		   	$phone_number  = $phone_numbers_res_data[0]->number;
		   	$phone_1 = substr($phone_number, 0, 3);
            $phone_2 = substr($phone_number, 3, 3);
            $phone_3 = substr($phone_number, 6, 4);

            $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;
		   	$ban_id  = $phone_numbers_res_data[0]->ban_id;
		}
		$data['ban_id'] = $ban_id;
		$data['phone_number'] = $phone_number;

		
		$banRes = DB::table('ban')->where('id', '=', $ban_id)->first();
		if (!empty($banRes) ) {
			$data['ban_number'] =  $banRes->number;
		}

		// Phone Data_log Records
		$phone_numbers_list_data = DB::table('data_log')->where('phone_number_id', '=', $phone_number_id)->orderBy('channelSeizureDate', 'desc');

		if ($phone_numbers_list_data->count() > 0) {
		   	$phone_numbers_res_data = $phone_numbers_list_data->get()->toArray();
		   	$data['phone_numbers_list_data'] = $phone_numbers_res_data;
		   	foreach ($phone_numbers_res_data as $value) {
		   		$data_log_ratedAmount += $value->ratedAmount;
				$data_log_callVolume += $value->callVolume;
		   	}
		} else {
			$data['phone_numbers_list_data'] = array();
		}

		// Phone Text_log Records
		$phone_numbers_list_text = DB::table('text_log')->where('phone_number_id', '=', $phone_number_id)->orderBy('channelSeizureDate', 'desc');

		if ($phone_numbers_list_text->count() > 0) {
		   	$phone_numbers_res_text = $phone_numbers_list_text->get()->toArray();
		   	$data['phone_numbers_list_text'] = $phone_numbers_res_text;
		   	foreach ($phone_numbers_res_text as $value) {
		   		$text_log_ratedAmount += $value->ratedAmount;
				$phone_number_texts++;
		   	}
		} else {
			$data['phone_numbers_list_text'] = array();
		}


		// Phone Text_log Records
		$phone_numbers_list_voice = DB::table('voice_log')->where('phone_number_id', '=', $phone_number_id)->orderBy('channelSeizureDate', 'desc');

		if ($phone_numbers_list_voice->count() > 0) {
		   	$phone_numbers_res_voice = $phone_numbers_list_voice->get()->toArray();
		   	$data['phone_numbers_list_voice'] = $phone_numbers_res_voice;
		   	foreach ($phone_numbers_res_voice as $value) {
		   		$voice_log_num++;
				$voice_log_ratedAmount += $value->ratedAmount;
				$voice_log_callDuration += $value->callDuration;
		   	}
		} else {
			$data['phone_numbers_list_voice'] = array();
		}

		$data['voice_log_num'] = $voice_log_num;
		$data['voice_log_ratedAmount'] = $voice_log_ratedAmount;
		$data['voice_log_callDuration'] = $voice_log_callDuration;
		$data['text_log_ratedAmount'] = $text_log_ratedAmount;
		$data['phone_number_texts'] = $phone_number_texts;
		$data['data_log_ratedAmount'] = $data_log_ratedAmount;
		$data['data_log_callVolume'] = $data_log_callVolume;

    	return View::make('pages.phone-number-detail',$data);

    }


    protected function checkLoggedin($request)
    {
    	// print_r($request->session()->get('isloggedin'));
        if(!$request->session()->get('isloggedin')){
            redirect('pages.login');
            die();
        }
    }
}
