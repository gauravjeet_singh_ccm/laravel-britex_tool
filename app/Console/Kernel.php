<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Illuminate\Support\Facades\Log;
use App\Model\CronLog;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //'App\Console\Commands\tmobileCycle',
        //'App\Console\Commands\CycleCron',
        //'App\Console\Commands\testCronJob',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) 
    {
        $minutes = env("CRON_INTERVAL",15);

        // $schedule->command('testcronjob:cron')
                // ->cron($minutes.' * * * *');

        // $schedule->command('testcronjob:cron')->everyMinute()->name('some_name')->withoutOverlapping();  

        // $schedule->command('testcronjob:cron')->everyFiveMinutes();
        // $schedule->command('testcronjob:cron')->everyTenMinutes();

        // $schedule->command('testcronjob:cron')
        //          ->cron('2 * * * *'); //cron('1 * * * *');
        // $schedule->command('testcronjob_half_hour:cron')
        //          ->cron('30 * * * *'); //cron('1 * * * *');
        // $schedule->command('testcronjob_quarter:cron')
        //          ->cron('45 * * * *'); //cron('1 * * * *');
        // $schedule->command('testcronjob_hour:cron')
        //          ->cron('* 1 * * *'); //cron('1 * * * *');
        // $schedule->command('testcronjob_hour_15:cron')
        //          ->cron('15 1 * * *'); //cron('1 * * * *');
        // $schedule->command('testcronjob_hour_30:cron')
        //          ->cron('30 1 * * *'); //cron('1 * * * *');
        // $schedule->call(function () {
        //     Log::info(': Job Run Successfully. ');
        // })->cron(' * * * *');

        $schedule->call('App\Http\Controllers\ApiCronController@getAllLines')->cron('*/'.$minutes.' * * * *');

    }



    // * * * * * php /var/www/tmobile-api/artisan testcronjob:cron 1>> /dev/null 2>&1

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
