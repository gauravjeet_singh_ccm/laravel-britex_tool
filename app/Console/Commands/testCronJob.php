<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Log;

use GuzzleHttp\Client;
use GuzzleHttp\Promise as GuzzlePromise;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;

// database access
use Illuminate\Support\Facades\DB;

use Guzzle\Common\Exception\MultiTransferException;
// use GuzzleHttp\Exception\ConnectException;

use GuzzleHttp\RequestOptions\CONNECT_TIMEOUT;



class testCronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testcronjob:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();


        $resutCount = 0;

        $request_per_minute = env("REQUESTS_PER_MINUTE",40);


        $cycle_id = 2;

        $base = config('app.goknowsAPI.api_url').'lines/';


        $newClient = new  \GuzzleHttp\Client(['base_uri' => $base]);
        // $resutCount =1;
        $requestPromises = [];


        $interval_res = DB::table('requests_intervals')->orderBy('created_at', 'desc')->first();


        if (!isset($interval_res->id)) {
            $interval_number = 1;
            $requests_intervals_data = array(
                'cronjob_status' => 1,
                'created_at' => date('Y-m-d H:i:s')
            );

            DB::table('requests_intervals')->insert($requests_intervals_data);  // result "true"
            $interval_id = DB::getPdo()->lastInsertId();
            // dd($id);
            // echo " INSERTED interval ID > ".  $interval_id."<br>";

            $interval_res = DB::table('requests_intervals')->where('id',$interval_id)->orderBy('created_at', 'desc')->first();

            $interval_id = $interval_res->id;
            $cronjob_status = $interval_res->cronjob_status;

        } else {
            $interval_row = $interval_res;
            $interval_id = $interval_row->id;
            $cronjob_status = $interval_row->cronjob_status;
        }


        // echo $cronjob_status;

        if($cronjob_status == 1) {

            $requests_intervals_data = array(
                'cronjob_status' => 0
            );

            DB::table('requests_intervals')->where('id',$interval_id)->update($requests_intervals_data);  // result "true"
            // $interval_id = DB::getPdo()->lastInsertId();

            // echo  'CRonJob Run: INTERVAL ID '. $interval_id;



            // Log::info('CRonJob Run: TestCronJob ');

            $responseLines = $client->request('GET', config('app.goknowsAPI.api_url').'lines', [
                'headers' => [
                    'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                ]
            ]);
            $result= $responseLines->getBody();
            $phone_data = json_decode($result);


            // print_r($phone_data);
            // die();

            $cycle_id = 2;

            $totalApiNumbers = count($phone_data);


            if($phone_data) {

                // echo $totalApiNumbers." Phone numbers";


                foreach ($phone_data as $key => $value) {

                    $resutCount++;

                    $phone_number =  $value->phone_number;

                    $ban_number =  $value->ban;


                    $interval_numbers_count = DB::table('interval_phone_numbers')->where('phone_interval_status',1)->count();

                    if($interval_numbers_count >= $totalApiNumbers){

                        $status_update_data = array( 'phone_interval_status' => 0 );

                        DB::table('interval_phone_numbers')->update($status_update_data);

                    }

                    $interval_phone_numbers_res = DB::table('interval_phone_numbers')->where('phone_number',$phone_number);

                    if($interval_phone_numbers_res->count() == 0){
                        // echo "Add PHone number";
                        $interval_phone_data = array(
                            'phone_number' => $phone_number,
                            'phone_interval_status' => 1,
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        DB::table('interval_phone_numbers')->insert($interval_phone_data);  // result "true"
                        $interval_id = DB::getPdo()->lastInsertId();

                        // echo "phone Number added<br>";


                    } else {

                        $interval_rec = $interval_phone_numbers_res->get()->first();

                        // echo $interval_rec->phone_interval_status;

                        if($interval_rec->phone_interval_status == 0){

                            $phon_update_interval = array('phone_interval_status' => 1);

                            DB::table('interval_phone_numbers')->where('phone_number', $interval_rec->phone_number)->update($phon_update_interval);

                        } else {

                            // echo "Continue<br>";

                            $resutCount--;

                            continue;

                        }

                    }

                    if($resutCount == $request_per_minute) {
                        break;
                    }


                    $requestPromises[$phone_number] = $newClient->getAsync( $phone_number.'/usage_details', [
                            'timeout' => 400, // Response timeout
                            // 'connect_timeout' => 20, // Connection timeout
                            'headers' => [
                                'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                                'Accept' => 'application/json; charset=utf-8'
                            ],
                            // 'form_params' => [
                            //     'from_date' => '2019-10-11',
                            //     'until_date' => '2019-11-10'
                            // ]
                        ]
                    );

                    /**
                    * Add Phone Number and Ban number in database
                    **/


                    $ban_res = DB::table('ban')->where('cycle_id', '=', $cycle_id)->where('number', '=', $ban_number);

                    if ($ban_res->count() == 0) {

                        $ban_data = array(
                            'cycle_id' => $cycle_id,
                            'number' => $ban_number
                        );

                        DB::table('ban')->insert($ban_data);  // result "true"
                        $ban_id = DB::getPdo()->lastInsertId();
                        // dd($id);
                        // echo " INSERTED Ban ID > ".  $ban_id."<br>";

                    } else {
                        $ban_row = $ban_res->get()->toArray();
                        $ban_id = $ban_row[0]->id;
                        // echo " FETCHED Ban ID > ".   $ban_id."<br>";
                    }

                    $phone_number_res = DB::table('phone_number')->where('cycle_id', '=', $cycle_id)->where('number', '=', $phone_number);


                    // $phone_number_res = DB::table('phone_number')->where('cycle_id', '=', 1)->where('ban_id', '=', $ban_id)->where('number', '=', $phone_number);
                    if ($phone_number_res->count() == 0) {
                        // echo "PHONE NUMBER ADDED!";

                        $phone_number_data = array(
                            'cycle_id' => $cycle_id,
                            'number' => $phone_number,
                            'ban_id' => $ban_id
                        );

                        DB::table('phone_number')->insert($phone_number_data);  // result "true"
                        // $phone_number_id = DB::getPdo()->lastInsertId();
                        // dd($id);
                    }
                    // $resutCount++;
                    // Log::info('Phone Number: '.$phone_number);
                    // $resutCount++;
                }


            }

            Log::info('CRonJob Run: TestCronJob '.$resutCount);
            // echo 'CRonJob Run: TestCronJob '.$resutCount;


            // echo " FETCHED Ban ID > ".   $ban_id."<br>";


            // echo $resutCount; die();
            // Wait for the requests to complete; throws a ConnectException
            // if any of the requests fail

            $results = GuzzlePromise\unwrap($requestPromises);

            // Wait for the requests to complete, even if some of them fail
            $results = GuzzlePromise\settle($requestPromises)->wait();


            // $results = GuzzlePromise\settle($requestPromises)->wait();

            foreach ($results as $key => $result) {

                Log::info('Phone: '.$key);

                // Log::info('State: '.$result['state']);

                if ($result['state'] === 'fulfilled') {
                    $response = $result['value'];

                    Log::info('ResponseCode: '.$response->getStatusCode());


                    // if ($response->getStatusCode() == 200 OR $response->getStatusCode() == 502 ) {
                    if ($response->getStatusCode() != 400 AND $response->getStatusCode() != 404 AND $response->getStatusCode() != 500 ) {

                        $resultUsage =$response->getBody();

                        $phone_data_Usage = json_decode($resultUsage);

                        if($phone_data_Usage != NULL){
                            $phone_data_Usage = get_object_vars($phone_data_Usage);
                        } else {
                            $phone_data_Usage = '';
                        }
                        $cycle_id = 2;


                        if(isset($phone_data_Usage) AND !empty($phone_data_Usage)) {

                            $phone_number = $key;
                            $ban_number = $phone_data_Usage['financialAccountNumber'];

                            $ban_res = DB::table('ban')->where('cycle_id', '=', $cycle_id)->where('number', '=', $ban_number);

                            if ($ban_res->count() == 0) {

                                $ban_data = array(
                                    'cycle_id' => $cycle_id,
                                    'number' => $ban_number
                                );

                                DB::table('ban')->insert($ban_data);  // result "true"
                                $ban_id = DB::getPdo()->lastInsertId();
                                // dd($id);
                                // echo " INSERTED Ban ID > ".  $ban_id."<br>";

                            } else {
                                $ban_row = $ban_res->get()->toArray();
                                $ban_id = $ban_row[0]->id;
                                // echo " FETCHED Ban ID > ".   $ban_id."<br>";
                            }

                            $phone_number_res = DB::table('phone_number')->where('cycle_id', '=', $cycle_id)->where('number', '=', $phone_number);


                            // $phone_number_res = DB::table('phone_number')->where('cycle_id', '=', 1)->where('ban_id', '=', $ban_id)->where('number', '=', $phone_number);
                            if ($phone_number_res->count() == 0) {
                                // echo "PHONE NUMBER ADDED!";

                                $phone_number_data = array(
                                    'cycle_id' => $cycle_id,
                                    'number' => $phone_number,
                                    'ban_id' => $ban_id
                                );

                                DB::table('phone_number')->insert($phone_number_data);  // result "true"
                                $phone_number_id = DB::getPdo()->lastInsertId();
                                // dd($id);
                            }else {
                                // echo "PHONE ALREADY EXIST!";
                                $phone_number_row = $phone_number_res->get()->toArray();
                                // print_r($phone_number_row);
                                $phone_number_id = $phone_number_row[0]->id;
                            }


                            if(isset($phone_data_Usage['error'])){

                                Log::info('Showing Error : '. $phone_data_Usage['error']);

                                $itterationFailed++;

                                $error = $phone_data_Usage['error'];
                                $title = $phone_data_Usage['title'];
                                $message = $phone_data_Usage['message'];
                                $code = $phone_data_Usage['code'];
                                $status = $phone_data_Usage['status'];

                                // echo "Error: ".$error." \nTitle:" .$title." \nMessage: ".$message." \nCode:". $code."\n\n\n";
                                continue;
                            } else {

                                $failed_records = 0;


                                $ban_number_new[] =  $phone_data_Usage['financialAccountNumber'];

                                $data_log_ratedAmount = 0;
                                $data_log_callVolume = 0;
                                $phone_number_texts = 0;
                                $text_log_ratedAmount = 0;
                                $voice_log_ratedAmount = 0;
                                $voice_log_num = 0;
                                $voice_log_callDuration =0;

                                // print_r($phone_data_Usage['data']);
                                Log::info('Data Count: '. count($phone_data_Usage['data']));
                                foreach ($phone_data_Usage['data'] as $key => $valueUsage)
                                {

                                    // echo "<h1>DATA</h1>";

                                    $channelSeizureDate = isset($valueUsage->channelSeizureDate) ? $valueUsage->channelSeizureDate : '';
                                    $callType = isset($valueUsage->callType) ? $valueUsage->callType : '';
                                    $bucket = isset($valueUsage->bucket) ? $valueUsage->bucket : '';
                                    if(!empty($bucket)) {
                                        $bucketService = $bucket[0]->bucketService;
                                        $bucketFeature = $bucket[0]->bucketFeature;
                                    } else {
                                        $bucketService = '';
                                        $bucketFeature = '';
                                    }
                                    $ratedAmount = isset($valueUsage->ratedAmount) ? $valueUsage->ratedAmount : '';
                                    $callVolume = isset($valueUsage->callVolume) ? $valueUsage->callVolume : '';
                                    $messageType = isset($valueUsage->messageType) ? $valueUsage->messageType : '';
                                    $roamingIndicator = isset($valueUsage->roamingIndicator) ? $valueUsage->roamingIndicator : '';
                                    $unitOfMeasure = isset($valueUsage->unitOfMeasure) ? $valueUsage->unitOfMeasure : '';
                                    $usageCategory = isset($valueUsage->usageCategory) ? $valueUsage->usageCategory : '';

                                    $data_log = array(
                                        'phone_number_id' => $phone_number_id,
                                        'channelSeizureDate' => $channelSeizureDate,
                                        'callType' => $callType,
                                        'bucketService' => $bucketService,
                                        'bucketFeature' => $bucketFeature,
                                        'ratedAmount' => $ratedAmount,
                                        'callVolume' => $callVolume,
                                        'messageType' => $messageType,
                                        'roamingIndicator' => $roamingIndicator,
                                        'unitOfMeasure' => $unitOfMeasure,
                                        'usageCategory' => $usageCategory,
                                        'created_at' => date('Y-m-d H:i:s')
                                    );

                                    // DB::table('data_log')->insert($data_log);  // result "true"

                                    $data_log_chck = DB::table('data_log')
                                                    ->where('channelSeizureDate', '=', $channelSeizureDate)
                                                    ->where('phone_number_id', '=', $phone_number_id)

                                                    ->where('bucketFeature', '=', $bucketFeature)
                                                    ->where('ratedAmount', '=', $ratedAmount)
                                                    ->where('callVolume', '=', $callVolume)
                                                    ->where('messageType', '=', $messageType)
                                                    ->where('roamingIndicator', '=', $roamingIndicator)
                                                    ->where('unitOfMeasure', '=', $unitOfMeasure)
                                                    ->where('usageCategory', '=', $usageCategory);

                                    if ($data_log_chck->count() > 0) {
                                        // $data_log_row = $data_log_chck->get()->toArray();
                                        // $data_log_id = $data_log_row[0]->id;
                                        // Log::info('DATA_log_id: ' . $data_log_id.' phone_number_id: '.$data_log_row[0]->phone_number_id);
                                        // continue;

                                        // echo "<pre>";
                                     //     print_r($data_log_chck->get());
                                        // echo "</pre>";

                                    } else {
                                        $data_log_res =  DB::table('data_log')->insert($data_log);  // result "true"
                                    }

                                }

                                Log::info('TEXT Count: '. count($phone_data_Usage['text']));

                                foreach ($phone_data_Usage['text'] as $key => $valueUsage)
                                {

                                    // echo "<h1>TEXT</h1>";


                                    $callType = isset($valueUsage->callType) ? $valueUsage->callType : '';
                                    $channelSeizureDate = isset($valueUsage->channelSeizureDate) ? $valueUsage->channelSeizureDate : '';
                                    $destinationCity = isset($valueUsage->destinationCity) ? $valueUsage->destinationCity : '';
                                    $destinationNumber = isset($valueUsage->destinationNumber) ? $valueUsage->destinationNumber : '';
                                    $destinationState = isset($valueUsage->destinationState) ? $valueUsage->destinationState : '';
                                    $direction = isset($valueUsage->direction) ? $valueUsage->direction : '';
                                    $bucket = isset($valueUsage->bucket) ? $valueUsage->bucket : '';
                                    if(!empty($bucket)) {
                                        $bucketService = $bucket[0]->bucketService;
                                        $bucketFeature = $bucket[0]->bucketFeature;
                                    } else {
                                        $bucketService = '';
                                        $bucketFeature = '';
                                    }
                                    $ratedAmount = isset($valueUsage->ratedAmount) ? $valueUsage->ratedAmount : '';
                                    $messageType = isset($valueUsage->messageType) ? $valueUsage->messageType : '';
                                    $billLegend = isset($valueUsage->billLegend) ? $valueUsage->billLegend : '';
                                    $origin = isset($valueUsage->origin) ? $valueUsage->origin : '';
                                    $roamingIndicator = isset($valueUsage->roamingIndicator) ? $valueUsage->roamingIndicator : '';
                                    $unitOfMeasure = isset($valueUsage->unitOfMeasure) ? $valueUsage->unitOfMeasure : '';
                                    $usageCategory = isset($valueUsage->usageCategory) ? $valueUsage->usageCategory : '';

                                    $text_log = array(
                                        'callType' => $callType,
                                        'phone_number_id' => $phone_number_id,
                                        'channelSeizureDate' => $channelSeizureDate,
                                        'destinationCity' => $destinationCity,
                                        'destinationNumber' => $destinationNumber,
                                        'destinationState' => $destinationState,
                                        'direction' => $direction,
                                        'bucketService' => $bucketService,
                                        'bucketFeature' => $bucketFeature,
                                        'ratedAmount' => $ratedAmount,
                                        'messageType' => $messageType,
                                        'billLegend' => $billLegend,
                                        'origin' => $origin,
                                        'roamingIndicator' => $roamingIndicator,
                                        'unitOfMeasure' => $unitOfMeasure,
                                        'usageCategory' => $usageCategory,
                                        'created_at' => date('Y-m-d H:i:s')
                                    );

                                    // DB::table('text_log')->insert($text_log);  // result "true"

                                    $text_log_chck = DB::table('text_log')
                                                    ->where('channelSeizureDate', '=', $channelSeizureDate)
                                                    ->where('destinationNumber','=', $destinationNumber)
                                                    ->where('phone_number_id', '=', $phone_number_id)

                                                    ->where('direction', '=', $direction)
                                                    ->where('ratedAmount', '=', $ratedAmount)
                                                    ->where('origin', '=', $origin)
                                                    ->where('messageType', '=', $messageType)
                                                    ->where('roamingIndicator', '=', $roamingIndicator)
                                                    ->where('unitOfMeasure', '=', $unitOfMeasure)
                                                    ->where('usageCategory', '=', $usageCategory);

                                    if ($text_log_chck->count() > 0) {
                                        // $text_log_row = $text_log_chck->get()->toArray();
                                        // $text_log_id = $text_log_row[0]->id;
                                        // Log::info('TEXT_log_id: ' . $text_log_id.' phone_number_id: '.$text_log_row[0]->phone_number_id);

                                        // continue;
                                    } else {
                                        $text_log_res =  DB::table('text_log')->insert($text_log);  // result "true"
                                    }

                                }

                                Log::info('VOICE Count: '. count($phone_data_Usage['voice']));

                                foreach ($phone_data_Usage['voice'] as $key => $valueUsage)
                                {

                                    // echo "<h1>VOICE</h1>";

                                    $callDuration = isset($valueUsage->callDuration) ? $valueUsage->callDuration : '';
                                    $callType = isset($valueUsage->callType) ? $valueUsage->callType : '';
                                    $carrier = isset($valueUsage->carrier) ? $valueUsage->carrier : '';
                                    $channelSeizureDate = isset($valueUsage->channelSeizureDate) ? $valueUsage->channelSeizureDate : '';
                                    $destinationCity = isset($valueUsage->destinationCity) ? $valueUsage->destinationCity : '';
                                    $destinationNumber = isset($valueUsage->destinationNumber) ? $valueUsage->destinationNumber : '';
                                    $destinationState = isset($valueUsage->destinationState) ? $valueUsage->destinationState : '';
                                    $originCity = isset($valueUsage->originCity) ? $valueUsage->originCity : '';
                                    $originState = isset($valueUsage->originState) ? $valueUsage->originState : '';
                                    $bucket = isset($valueUsage->bucket) ? $valueUsage->bucket : '';
                                    if(!empty($bucket)) {
                                        $bucketService = $bucket[0]->bucketService;
                                        $bucketFeature = $bucket[0]->bucketFeature;
                                    } else {
                                        $bucketService = '';
                                        $bucketFeature = '';
                                    }
                                    $ratedAmount = isset($valueUsage->ratedAmount) ? $valueUsage->ratedAmount : '';
                                    $billLegend = isset($valueUsage->billLegend) ? $valueUsage->billLegend : '';
                                    $messageType = isset($valueUsage->messageType) ? $valueUsage->messageType : '';
                                    $direction = isset($valueUsage->direction) ? $valueUsage->direction : '';
                                    $origin = isset($valueUsage->origin) ? $valueUsage->origin : '';
                                    $roamingIndicator = isset($valueUsage->roamingIndicator) ? $valueUsage->roamingIndicator : '';
                                    $unitOfMeasure = isset($valueUsage->unitOfMeasure) ? $valueUsage->unitOfMeasure : '';
                                    $usageCategory = isset($valueUsage->usageCategory) ? $valueUsage->usageCategory : '';

                                    $voice_log = array(
                                        'callDuration' => $callDuration,
                                        'phone_number_id' => $phone_number_id,
                                        'callType' => $callType,
                                        'carrier' => $carrier,
                                        'channelSeizureDate' => $channelSeizureDate,
                                        'destinationCity' => $destinationCity,
                                        'destinationNumber' => $destinationNumber,
                                        'destinationState' => $destinationState,
                                        'originCity' => $originCity,
                                        'originState' => $originState,
                                        'bucketService' => $bucketService,
                                        'bucketFeature' => $bucketFeature,
                                        'ratedAmount' => $ratedAmount,
                                        'billLegend' => $billLegend,
                                        'messageType' => $messageType,
                                        'direction' => $direction,
                                        'origin' => $origin,
                                        'roamingIndicator' => $roamingIndicator,
                                        'unitOfMeasure' => $unitOfMeasure,
                                        'usageCategory' => $usageCategory,
                                        'created_at' => date('Y-m-d H:i:s')
                                    );

                                    $voice_log_chck = DB::table('voice_log')
                                                    ->where('channelSeizureDate', '=', $channelSeizureDate)
                                                    ->where('destinationNumber','=', $destinationNumber)
                                                    ->where('phone_number_id', '=', $phone_number_id)

                                                    ->where('destinationCity',   '=', $destinationCity)
                                                    ->where('destinationNumber',     '=', $destinationNumber)
                                                    ->where('destinationState',  '=', $destinationState)
                                                    ->where('originCity',    '=', $originCity)
                                                    ->where('originState',   '=', $originState)
                                                    ->where('bucketService',     '=', $bucketService)
                                                    ->where('bucketFeature',     '=', $bucketFeature)
                                                    ->where('ratedAmount',   '=', $ratedAmount);

                                    if ($voice_log_chck->count() > 0) {
                                        // $voice_log_row = $voice_log_chck->get()->toArray();
                                        // $voice_log_id = $voice_log_row[0]->id;
                                        // Log::info('VOICE_log_id: ' . $voice_log_id.' phone_number_id: '.$voice_log_row[0]->phone_number_id);
                                        // continue;
                                    } else {
                                        $voice_log_res =  DB::table('voice_log')->insert($voice_log);  // result "true"
                                    }

                                }

                                if($response->getStatusCode() === 200) {

                                    /*
                                    * Matche records that are matching with database or not.
                                    **/
                                    $phone_number_data_log_res = DB::table('data_log')->where('phone_number_id', '=', $phone_number_id);
                                    if($phone_number_data_log_res->count() > 0 AND $phone_number_data_log_res->count() != count($phone_data_Usage['data']) ) {
                                        $failed_records = 1;

                                        Log::info('DATA Records Not Matched: '. $phone_number . ' Phone_number_id: '.$phone_number_id.' Existing: '.$phone_number_data_log_res->count()." New: ".count($phone_data_Usage['data']) );
                                    }

                                    $phone_number_text_log_res = DB::table('text_log')->where('phone_number_id', '=', $phone_number_id);
                                    if($phone_number_text_log_res->count() > 0 AND $phone_number_text_log_res->count() != count($phone_data_Usage['text']) ) {
                                        $failed_records = 1;

                                        Log::info('TEXT Records Not Matched: '. $phone_number . ' Phone_number_id: '.$phone_number_id.' Existing: '.$phone_number_text_log_res->count()." New: ".count($phone_data_Usage['text']) );
                                    }

                                    $phone_number_voice_log_res = DB::table('voice_log')->where('phone_number_id', '=', $phone_number_id);
                                    if($phone_number_voice_log_res->count() > 0 AND $phone_number_voice_log_res->count() != count($phone_data_Usage['voice']) ) {
                                        $failed_records = 1;

                                        Log::info('VOICE Records Not Matched: '. $phone_number . ' Phone_number_id: '.$phone_number_id.' Existing: '.$phone_number_voice_log_res->count()." New: ".count($phone_data_Usage['voice']) );
                                    }

                                    if($failed_records == 0) {
                                        Log::info('All Data Verified for Phone: '. $phone_number . ' Phone_number_id: '.$phone_number_id."\n\n" );
                                    } else {
                                        Log::info('Warning! DATA Mismatched for Phone: '. $phone_number . ' Phone_number_id: '.$phone_number_id."\n\n" );
                                    }
                                }
                            }

                            // Update data_log.ratedAmount and data_log.callVolume in tables
                            $data_log_call_volume = DB::table('data_log')
                            ->select(
                                DB::raw('SUM(callVolume) as data_log_callVolume'),
                                DB::raw('SUM(ratedAmount) as data_log_ratedAmount')
                            )
                            ->where('phone_number_id', $phone_number_id)
                            ->get()
                            ->toArray();

                            $data_log_ratedAmount = $data_log_call_volume[0]->data_log_ratedAmount;
                            $data_log_callVolume = $data_log_call_volume[0]->data_log_callVolume;

                            // $data_log_callVolume = $data_log_call_volume[0]->data_log_callVolume;

                            $text_log_call_volume = DB::table('text_log')
                            ->select(
                                DB::raw('COUNT(*) as phone_number_texts'),
                                DB::raw('SUM(ratedAmount) as text_log_ratedAmount')
                            )
                            ->where('phone_number_id', $phone_number_id)
                            ->get()
                            ->toArray();

                            $phone_number_texts = $text_log_call_volume[0]->phone_number_texts;
                            $text_log_ratedAmount = $text_log_call_volume[0]->text_log_ratedAmount;

                            // $phone_number_texts = $text_log_call_volume[0]->phone_number_texts;

                            $voice_log_call_volume = DB::table('voice_log')
                            ->select(
                                DB::raw('SUM(ratedAmount) as voice_log_ratedAmount'),
                                DB::raw('COUNT(*) as voice_log_num'),
                                DB::raw('SUM(callDuration) as voice_log_callDuration')
                            )
                            ->where('phone_number_id', $phone_number_id)
                            ->get()
                            ->toArray();

                            $voice_log_ratedAmount = $voice_log_call_volume[0]->voice_log_ratedAmount;
                            $voice_log_num = $voice_log_call_volume[0]->voice_log_num;
                            $voice_log_callDuration =$voice_log_call_volume[0]->voice_log_callDuration;

                            // $voice_log_callVolume = $voice_log_call_volume[0]->voice_log_callVolume;

                            $phone_update_arr = array(
                                'data_rated_amount' => $data_log_ratedAmount,
                                'data' => $data_log_callVolume,
                                'text_rated_amount' => $text_log_ratedAmount,
                                'texts' => $phone_number_texts,
                                'voice_rated_amount' => $voice_log_ratedAmount,
                                'voice_min' => $voice_log_callDuration,
                                'voice_num' => $voice_log_num
                            );
                            DB::table('phone_number')->where('number', $phone_number)->where('id', $phone_number_id)->where('cycle_id', $cycle_id)->update($phone_update_arr);

                            // Get Updated Data from particular phone number and update in Ban Table

                            $ban_phone_number_data = DB::table('phone_number')
                            ->select(
                                DB::raw('SUM(data_rated_amount) AS data_rated_amount'),
                                DB::raw('SUM(data) AS data'),
                                DB::raw('SUM(texts) AS texts'),
                                DB::raw('SUM(text_rated_amount) AS text_rated_amount'),
                                DB::raw('SUM(voice_rated_amount) AS voice_rated_amount'),
                                DB::raw('SUM(voice_min) AS voice_min'),
                                DB::raw('SUM(voice_num) AS voice_num')
                            )
                            ->where('ban_id', $ban_id)
                            ->get()
                            ->toArray();

                            // update `ban` table
                            $ban_update_arr = array(
                                'data_rated_amount' => $ban_phone_number_data[0]->data_rated_amount,
                                'data' => $ban_phone_number_data[0]->data,
                                'text_rated_amount' => $ban_phone_number_data[0]->text_rated_amount,
                                'texts' => $ban_phone_number_data[0]->texts,
                                'voice_rated_amount' => $ban_phone_number_data[0]->voice_rated_amount,
                                'voice_min' => $ban_phone_number_data[0]->voice_min,
                                'voice_num' => $ban_phone_number_data[0]->voice_num
                            );

                            DB::table('ban')->where('id', $ban_id)->where('cycle_id', $cycle_id)->update($ban_update_arr);

                            // echo "Phone Number: ".$phone_number." Ban: ".$ban_number."\n";

                            // $cron_run = true;
                        } else {
                            // continue;
                        }

                    } else {
                        $response->getStatusCode();
                    }
                } else if ($result['state'] === 'rejected') {
                    // notice that if call fails guzzle returns is as state rejected with a reason.
                    Log::info('ERR: REJECTED ' . $result['reason']);

                    // $site->setHtml('ERR: ' . $result['reason']);
                } else {
                    // $site->setHtml('ERR: unknown exception ');
                    Log::info('ERR: unknown exception');
                }

            }



            /* update DronJob Completed.*/

            $interval_res = DB::table('requests_intervals')->orderBy('created_at', 'desc')->first();

            if (isset($interval_res->id)) {
                $interval_id = $interval_res->id;
                $requests_intervals_data = array(
                    'cronjob_status' => 1,
                    'updatedAt' => date('Y-m-d H:i:s')
                );

                $status_udpate = DB::table('requests_intervals')->where('id',$interval_id)->update($requests_intervals_data);  // result "true"

                if($status_udpate) {
                    Log::info('CRonJob Ended: '.$resutCount." Update Cronjob Run Status ");

                }

            }

        } else {
            Log::info('CRonJob continue running '.$resutCount);

        }


    }
}
