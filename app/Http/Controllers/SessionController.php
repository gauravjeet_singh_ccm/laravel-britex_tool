<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use View;


class SessionController extends Controller {
   public function accessSessionData(Request $request) {
      if($request->session()->has('isloggedin')) {
         echo $request->session()->get('isloggedin');
      }
      else {
         echo 'No data in the session';
      }
   }
   public function storeSessionData(Request $request) {
   	
        // dd($request->post());
        $this->validateLogin($request);

	   	
	   	$username = $request->post('lg_username');
        $password = $request->post('lg_password');

        if($username AND $password ) {
            if($username == config('app.loginusername') AND $password == config('app.loginuserpassword')){
                $request->session()->put('isloggedin', 'user-loggedin');
                return redirect('/');
            } 
            else {
            	$data = array();
        		$data['error'] = 'Credentials not matching! please try again.';
    				return View::make('pages.home',$data);

	        }
        

        } 
      // echo "Data has been added to session";
   }
   public function deleteSessionData(Request $request) {
      $request->session()->forget('isloggedin');
      $request->session()->forget('cycle_id');

		return redirect('/');
		die(); 	
   }


    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            'lg_username' => 'required|string',
            'lg_password' => 'required|string',
        ]);
    }
}