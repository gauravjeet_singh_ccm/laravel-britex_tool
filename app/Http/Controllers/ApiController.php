<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Config;
use View;
use Illuminate\Support\Collection;

use GuzzleHttp\Client;
use GuzzleHttp\Promise as GuzzlePromise;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;



class ApiController extends Controller
{
    
    /**
    * Bulk Restore Lines
    * This function Restore lines in bulk and gives and order number to track
    */

    public function restoreBulkLines(Request $request)
    {
        $this->checkLoggedin($request);
        // dd($request->post('numbers_restore_lines'));
        $client = new Client();
            
        $jsonData = array("status" => "active");
        $lines = explode("\n", $request->post('numbers_restore_lines')); // or use PHP PHP_EOL constant
        if ( !empty($lines) ) {
            foreach ( $lines as $line ) {
                $phone_no = trim(str_replace('-','',$line));
                $jsonData['requests'][] =  array('phone_number' => $phone_no, "mno" => "tmo");
            }
        }
        
        /*-------------- PUT -> REQUEST FOR SUSPENDED AND Active Numbers  ---------------*/
            // dd($jsonDat1a);

        $header = array(
            'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
            "Accept" => "application/json"
        );
        
        $response = $client->put(config('app.goknowsAPI.api_url').'lines/bulk', array('headers' => $header, 'json' => $jsonData));
        
        // dd($response);

        $result= $response->getBody();

        $data=[];

        $data['result_bulk_restore'] = json_decode($result);

        // return All Numbers
        $data['result'] = $this->getAllLines();

        // dd($data);
    
        return View::make('pages.home',$data);
        
    }


    /**
     * Restore Multiple Lines (Numbers)
     *  for Multiple Response each Record.
     */

     public function restoreMultipleLines(Request $request)
     {
        $this->checkLoggedin($request);
        $request->flash();
        $numbers_restore_lines = $request->old('numbers_restore_lines');
         // dd($request->post('numbers_restore_lines'));
        $client = new Client();
        $data =[];
            
        $jsonData = array("status" => "active");
        $lines = explode("\n", $request->post('numbers_restore_lines')); // or use PHP PHP_EOL constant
        if ( !empty($lines) ) {
        
            foreach ( $lines as $line ) {
                $phone_no = trim(str_replace('-','',$line));
                $numbers =  array('phone_number' => trim($line), "mno" => "tmo");

                $phone_1 = substr($phone_no, 0, 3);
                $phone_2 = substr($phone_no, 3, 3);
                $phone_3 = substr($phone_no, 6, 4);

                $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;

                /*-------------- PUT -> REQUEST FOR RESTORE Numbers  ---------------*/
                // dd($jsonDat1a);
                
                $response = $client->request('put', config('app.goknowsAPI.api_url').'lines/'.$phone_no, [
                    'headers' => [
                        'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                        'Accept'     => 'application/json'
                    ],
                    'form_params' => [
                        'status' => 'active'
                    ]
                ]);
        
                $result= $response->getBody();
                $data['number'][] = $phone_number;
                $data['result_restore_line'][] = json_decode($result);

                // dd($data);
            }
        
        }

        // return All Numbers
        $data['result'] = $this->getAllLines();
        $data['targetId'] = $request->post('targetId');
        
        
        // dd($data);

        return View::make('pages.home',$data);

     }



    /**
     * Suspend Single Line
     */

    public function suspendline(Request $request)
    {
        // print_r($_POST);
        $this->checkLoggedin($request);

        $client = new Client();

        $res = $client->request('put', config('app.goknowsAPI.api_url').'lines/'.$request->post('numbers_suspend_lines'), [
            'headers' => [
                'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
            ],
            'form_params' => [
                'status' => 'suspended',
                'reason_code' => 'ELST'
            ]

        ]);

        $result= $res->getBody();
        $data = [];
        $data['result_suspend_line'] = json_decode($result);
        
        // dd($result);
        // return all Numbers
        $data['result'] = $this->getAllLines();
        
        return View::make('pages.home',$data);
        
    }



    /**
     * Suspend Multiple Lines via Textarea Multiple Lines
     *  for Multiple Response each Record.
     */

    public function suspendMultipleLines(Request $request)
    {   
        $this->checkLoggedin($request);
        
        // dd($request->post);
        $client = new Client();

        $request->flash();
        $numbers_suspend_lines = $request->old('numbers_suspend_lines');


        $lines = explode("\n", $request->post('numbers_suspend_lines')); // or use PHP PHP_EOL constant
        $data = [];
        if ( !empty($lines) ) {

            $header = array(
                'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                "Accept" => "application/json"
            );
            foreach ( $lines as $line ) {

                /*-------------- PUT -> REQUEST FOR SUSPENDED AND Active Numbers  ---------------*/
                $phone_no = trim(str_replace('-','',$line));

                $phone_1 = substr($phone_no, 0, 3);
                $phone_2 = substr($phone_no, 3, 3);
                $phone_3 = substr($phone_no, 6, 4);

                $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;

                
                $response = $client->put( config('app.goknowsAPI.api_url').'lines/'.$phone_no, array(
                    'headers' => $header,
                    'json' => array(
                        'status' => 'suspended',
                        'reason_code' => 'ELST',
                        'mno' => 'tmo'
                    )
                ));

                $result = $response->getBody();
                $data['number'][] = $phone_number;
                $data['result_suspend_lines'][] = json_decode($result);
            }
        }

        // return all Numbers
        $data['result'] = $this->getAllLines();
        $data['targetId'] = $request->post('targetId');
        
        // dd($data);
        return View::make('pages.home',$data);
        
    }



    /*
     * Suspend Bulk Lines for Single Response.
     */

    public function suspendBulkline(Request $request)
    {
        $this->checkLoggedin($request);

        // print_r($_POST);
        $client = new Client();
            
        $jsonData = array("status" => "suspended", "reason_code"=>"ELST");
        $lines = explode("\n", $request-post('numbers_suspend_lines')); // or use PHP PHP_EOL constant
        $data = [];
    
        if ( !empty($lines) ) {
            $header = array(
                'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                "Accept" => "application/json"
            );
            foreach ( $lines as $line ) {
                $phone_no = trim(str_replace('-','',$line));
                $jsonData['requests'][] =  array('phone_number' => $phone_no, "mno" => "tmo");
            }
        }

        /*-------------- PUT -> REQUEST FOR SUSPENDED AND Active Numbers  ---------------*/

        $response = $client->put(config('app.goknowsAPI.api_url').'lines/'.trim($line), array('headers' => $header, 'form_params' => $jsonData));

        $result= $response->getBody();

        $data['result_suspended_bulk'] = json_decode($result);
        
        // return all Numbers
        $data['result'] = $this->getAllLines();
        $data['targetId'] = $request->post('targetId');

        return View::make('pages.home',$data);
        
    }


    /**
    *  Activate Bulk CSV Upload Function
    **/

    public function bulkActivateLineCsvUpload(Request $request)
    {
        $this->checkLoggedin($request);

        $client = new Client();

        // get csv file
        $handle_csv = $request->file('bulk_active_line_csv');

        $filePath = $handle_csv->getRealPath();

        // read csv file
        $handle = fopen($filePath, "r");

        $header = true;

        // iterate csv file
        while ($csvLine = fgetcsv($handle, 1000, ",")) {

            if ($header) {
                $header = false;
            } else {
                $sim_number = trim(str_replace('F','',$csvLine[0]));

                if($sim_number ==""){
                    continue;
                }
                $data['sim_number'][] = $sim_number;
                
                $active_sims_data = array(
                        'sim_number' => $sim_number,
                        'plan_name' => 'unlimited',
                        "area_code" => "404",
                        "mno"  => "tmo"
                    );

                $headers = array(
                    'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                    "Accept" => "application/json"
                );

                try {
                    $response =  $client->post(config('app.goknowsAPI.api_url').'lines/', array('headers' => $headers, 'form_params' => array('status' => 'activated', 'requests' => $active_sims_data) ) );  
                    $data['result_activate_lines'][] = json_decode($response->getBody());
                }
                catch (GuzzleHttp\Exception\ClientException $e) {
                    $response[] = (\GuzzleHttp\Psr7\str($e->getResponse()));
                }
            }
        }

        // return all Numbers
        $data['result'] = $this->getAllLines();
        $data['targetId'] = $request->post('targetId');


        return View::make('pages.home',$data);

    }
    

    
    /**
    *  SIM Swap CSV Upload Function
    **/

    public function csvUploadBulkSwapChangeSims(Request $request)
    {
        $this->checkLoggedin($request);

        $client = new Client();
        header('Content-Type: text/html; charset=UTF-8');

        // get csv file
        $handle_csv = $request->file('csv_upload');
        $area_phone_number = $request->post('phone_number');

        $result = array();
        if( isset($handle_csv) AND !empty($handle_csv) ) {



            $filePath = $handle_csv->getRealPath();

            // read csv file
            $handle = fopen($filePath, "r");

            $header = true;
            $data=[];

            $data['targetId'] = $request->post('targetId');

            // iterate csv file
            while ($csvLine = fgetcsv($handle, 2000, ",")) {

                if ($header) {
                    $header = false;
                } else {    
                    
                    if(count($csvLine) ==1 ) continue;

                    $phone_no = trim(str_replace('-','',$csvLine[0]) );
                    // $sim_number = trim($csvLine[1]);
                    $sim_number = trim(str_replace('F','',$csvLine[1]));
                    if(empty($phone_no) || empty($sim_number) ){
                        continue;
                    }

                    
                    $phone_1 = substr($phone_no, 0, 3);
                    $phone_2 = substr($phone_no, 3, 3);
                    $phone_3 = substr($phone_no, 6, 4);

                    $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;
                    

                    $data['number'][] = $phone_number;

                    // dd($phone_no);
                    $swap_sims_change_Data = array('sim_number' => $sim_number, "mno" => "tmo");
                    // dd($swap_sims_change_Data);
                    $headers = array(
                        'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                        "Accept" => "application/json"
                    );

                    // dd(config('app.goknowsAPI.api_url').'lines/'.$phone_no);

                    // $response = $client->put(config('app.goknowsAPI.api_url').'lines/'.$phone_no, array('headers' => $headers, 'form_params' =>  $swap_sims_change_Data) );
                    $response = $client->put(config('app.goknowsAPI.api_url').'lines/'.$phone_no, array('headers' => $headers, 'json' =>  $swap_sims_change_Data) );


                    $result[] = json_decode($response->getBody());

                }
                
            }
        }
        else if(isset($area_phone_number) AND !empty($area_phone_number)) {

            // iterate Phone Numbers
            $i=0;
            foreach ($area_phone_number as $number) {
                
                $phone_no = trim(str_replace('-','',$number) );
                $sim_number = trim(str_replace('F','',$request->post('sim_number')[$i]));

                if(empty($phone_no) || empty($sim_number) ){
                    continue;
                }
                // echo $phone_no." SIM: <> PHONE:".$sim_number."<br>";

                
                $phone_1 = substr($phone_no, 0, 3);
                $phone_2 = substr($phone_no, 3, 3);
                $phone_3 = substr($phone_no, 6, 4);

                $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;
                

                $data['number'][] = $phone_number;

                // dd($phone_no);
                $swap_sims_change_Data = array('sim_number' => $sim_number, "mno" => "tmo");
                // dd($swap_sims_change_Data);
                $headers = array(
                    'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                    "Accept" => "application/json"
                );

                // dd(config('app.goknowsAPI.api_url').'lines/'.$phone_no);

                // $response = $client->put(config('app.goknowsAPI.api_url').'lines/'.$phone_no, array('headers' => $headers, 'form_params' =>  $swap_sims_change_Data) );
                $response = $client->put(config('app.goknowsAPI.api_url').'lines/'.$phone_no, array('headers' => $headers, 'json' =>  $swap_sims_change_Data) );


                $result[] = json_decode($response->getBody());

          
                
                $i++;
            }
                

            
        }
        
        $data['result_swap_sims'] = $result;
        // dd($data);  
        $data['targetId'] = $request->post('targetId');


        // return all Numbers
        
        $data['result'] = $this->getAllLines();

        return View::make('pages.home',$data);
    }


    /**
    *  Area Code Changes CSV Upload Function
    **/

    public function csvUploadBulkAreacodeChangeSims(Request $request)
    {
        $this->checkLoggedin($request);


        $client = new Client();

        // get csv file
        $handle_csv = $request->file('csv_upload_areacode');
        $area_phone_number = $request->post('area_phone_number');

        if (isset($handle_csv) AND !empty($handle_csv) ) {
            

            $filePath = $handle_csv->getRealPath();

            // read csv file
            $handle = fopen($filePath, "r");

            $header = true;
            
            $data=[];


            // iterate csv file
            while ($csvLine = fgetcsv($handle, 2000, ",")) {

                if ($header) {
                    $header = false;
                } else {
                    
                    $phone_no = trim(str_replace('-','',$csvLine[0]));
                    $areacode = trim($csvLine[1]);

                    // $sim_number = trim($csvLine[1]);
                    if($phone_no ==""){
                        continue;
                    }

                    
                    $phone_1 = substr($phone_no, 0, 3);
                    $phone_2 = substr($phone_no, 3, 3);
                    $phone_3 = substr($phone_no, 6, 4);

                    $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;

                    
                    $data['number'][] = $phone_number;
                    
                    $simData = array('area_code' => $areacode );

                    $headers = array(
                        'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                        "Accept" => "application/json"
                    );

                    // dd($simData);

                    $response = $client->put(config('app.goknowsAPI.api_url').'lines/'.$phone_no, array( 'headers' => $headers, 'json' =>  $simData) );

                    $data['result_areacode_change'][] = json_decode($response->getBody());
                }

            }

        } else if (isset($area_phone_number) AND !empty($area_phone_number) ) {
            // iterate csv file
            $i=0;
            foreach ($area_phone_number as $number) {
                
                $phone_no = trim(str_replace('-','',$number));
                $areacode = trim($request->post('areacode')[$i]);

                // $sim_number = trim($csvLine[1]);
                if(empty($phone_no)){
                    continue;
                }

                // echo $phone_no." <<<< ".$areacode."<br>";

                
                $phone_1 = substr($phone_no, 0, 3);
                $phone_2 = substr($phone_no, 3, 3);
                $phone_3 = substr($phone_no, 6, 4);

                $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;

                
                $data['number'][] = $phone_number;
                
                $simData = array('area_code' => $areacode );

                $headers = array(
                    'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                    "Accept" => "application/json"
                );

                // dd($simData);

                $response = $client->put(config('app.goknowsAPI.api_url').'lines/'.$phone_no, array( 'headers' => $headers, 'json' =>  $simData) );

                $data['result_areacode_change'][] = json_decode($response->getBody());
                $i++;
            
            }
            
        }

        // return all Numbers
        
        $data['result'] = $this->getAllLines();
        $data['targetId'] = $request->post('targetId');
        

        // dd($data);

        return View::make('pages.home',$data);
        
    }



    /**
     * Export data into CSV file
     */

    public function exportCsvFiles(Request $request)
    {
        $this->checkLoggedin($request);
        

        $client = new Client();

        // dd($request->post());
        $result = array();

        if($request->post('reports_in_csv')=="all"){

            $response = $client->get(config('app.goknowsAPI.api_url').'lines', [
                    'headers' => [
                        'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                        "Content-Type" => "application/json",
                    ]
                ]);
            $data['data-result'] = json_decode($response->getBody());

            /**
             * CSV Generate Code PHP
             */

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=all_phone_numbers('.date('m_d_Y').').csv');
            $output = fopen("php://output", "w");
            fputcsv($output,array('phone_number','sim_number','BAN','Status'));
            
            foreach($data['data-result'] as $value)
            {
                $phone_1 = substr($value->phone_number, 0, 3);
                $phone_2 = substr($value->phone_number, 3, 3);
                $phone_3 = substr($value->phone_number, 6, 4);

                $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;
                // Add Record to CSV file Row
                fputcsv($output,array(
                        $phone_number, 
                        $value->sim_number, 
                        $value->ban, 
                        $value->status
                    )
                );
            }
            fclose($output);
            die();

        }
        elseif($request->post('specific_numbers'))
        {
            $lines = explode("\n", $request->post('specific_numbers')); // or use PHP PHP_EOL constant
            if ( !empty($lines) ) {
                // $result=[];
            
                foreach ( $lines as $line ) {
                    $phone_no = trim(str_replace('-','',$line) );
                    /*-------------- GET -> REQUEST FOR GETTING ALL Numbers  ---------------*/
                    $response = $client->get(config('app.goknowsAPI.api_url').'lines/'.$phone_no, [
                        'headers' => [
                            'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                            "Accept" => "application/json"

                        ]
                    ]);
          
                    $result_check = json_decode($response->getBody());

                    // Exception Record Not found and error occurd in the given record
                    if(isset($result_check->message)){ continue; }

                    $result[] = json_decode($response->getBody());
                }

                $data['specific_numbers'] = $result;

                /**
                 * CSV Generate Code PHP
                 */

                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename=specific_numbers('.date('m_d_Y').').csv');
                $output = fopen("php://output", "w");
                fputcsv($output,array('phone_number','sim_number','BAN','Status'));
                
                foreach($data['specific_numbers'] as $value)
                {
                    $phone_1 = substr($value->phone_number, 0, 3);
                    $phone_2 = substr($value->phone_number, 3, 3);
                    $phone_3 = substr($value->phone_number, 6, 4);

                    $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;

                    // Add Record to CSV file Row
                    fputcsv($output,array(
                            $phone_number, 
                            $value->sim_number, 
                            $value->ban, 
                            $value->status
                        )
                    );
                }

                fclose($output);
                die();
            }
        }
        elseif($request->post('specific_sims'))
        {
            $lines = explode("\n", $request->post('specific_sims')); // or use PHP PHP_EOL constant
            
            if ( !empty($lines) ) {
            
                foreach ( $lines as $line ) {
                    
                    /*-------------- GET -> REQUEST FOR SIM Records  ---------------*/
                    
                    $response = $client->get(config('app.goknowsAPI.api_url').'lines?sim='.trim($line), [
                        'headers' => [
                            'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),
                            "Accept" => "application/json"

                        ]
                    ]);

                    $response_data = json_decode($response->getBody());
                    
                    // Error Exception if record not found. Skip record from Loop
                    
                    if(empty($response_data)) : continue; endif;
            
                    $result[] = json_decode($response->getBody());
                    
                }

                $data['specific_sims'] = $result;

                /**
                 * CSV Generate Code PHP
                 */

                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename=specific_sims('.date('m_d_Y').').csv');
                $output = fopen("php://output", "w");
                fputcsv($output,array('phone_number','sim_number','BAN','Status'));
             
                if($data['specific_sims']) {
                    foreach($data['specific_sims'] as $value)
                    {
                        foreach ($value as $value_data) {
                            // Add Record to CSV file Row
                            $phone_1 = substr($value_data->phone_number, 0, 3);
                            $phone_2 = substr($value_data->phone_number, 3, 3);
                            $phone_3 = substr($value_data->phone_number, 6, 4);

                            $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;
                            fputcsv($output,array(
                                    $phone_number, 
                                    $value_data->sim_number, 
                                    $value_data->ban, 
                                    $value_data->status
                                )
                            );
                        }
                    }
                }
        
                fclose($output);
                die();
                // dd($data);
            }
        }
    }
    
    /**
     * Get All Reords to show in all records
     */

    protected function getAllLines()
    {
        $client = new Client();
        /*-------------- GET -> All Numbers  ---------------*/
        // mno=att, mno=tmo
        $response = $client->request('GET', config('app.goknowsAPI.api_url').'lines', [
            'headers' => [
                'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),                
            ]
        ]);
        $result= $response->getBody();
        return json_decode($result);
    }

    /**
    * 
    * Get User Session Logged in.
    *
    */

    protected function checkLoggedin($request)
    {
        if(!$request->session()->get('isloggedin')){
            redirect('pages.login');
            die();
        }
    }

}

