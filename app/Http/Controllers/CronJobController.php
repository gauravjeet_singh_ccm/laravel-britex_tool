<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use App\Http\Requests;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Config;
use View;
use Illuminate\Support\Collection;

use GuzzleHttp\Client;
use GuzzleHttp\Promise as GuzzlePromise;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;

// database access
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class CronJobController extends Controller
{

    /**
     * CronJob Execute Callback Function
     */

    public function addCycles(){

        $date_year = 2037;

        for($i=2019; $i<=$date_year; $i++){
            $month = 12;
            for($j = 1; $j<=12; $j++){
                
                if($j < date('m')-1 AND $i <= 2019 ) {
                    continue;
                }

                $zero = ($j <= 9) ? '0' : '';
                
                $mon = ($j == 12) ? 1 : $j+1;

                $yr = ($j == 12) ? $i+1 : $i;

                // echo ($j==12) ? "this is 12th<hr>" : " else <hr>";
                // echo $yr."<br>";

                $startDate = $i."/".$j."/11 00:00:00";

                $endDate = $yr."/".$mon."/10 23:59:59";


                $date_start = strtotime($startDate);
                $data_start_conv = date('Y-m-d H:i:s', $date_start);

                $date_end = strtotime($endDate);
                $data_end_conv = date('Y-m-d H:i:s', $date_end);

                // echo $startDate." _____ ".$endDate."<br>";
                // echo $data_start_conv." _____ ".$data_end_conv."<hr>";


                $cycles = DB::table('cycle')->where('start_date', '>=', $data_start_conv)->where('end_date', '<=', $data_end_conv);

                if ($cycles->count() > 0) {
                    continue;

                    // echo "<pre>";
                    // print_r($cycles->get());
                    // echo "</pre>";

                } else {
                    $cycles =  DB::table('cycle')->insert(['start_date' => $data_start_conv, 'end_date' => $data_end_conv]);  // result "true"
                    // echo "<pre>";
                    // print_r($cycles->get());
                    // echo "</pre>";
                }
                
                
            }
        }
    }

    /**
     * Get Latest Ban Records
     *  
     */

    public function getBanLatestLines(Request $request)
    {
        $this->checkLoggedin($request);
        // dd($data);

        $data = array();



        $billing_cycle_list = DB::table('cycle')->get()->toArray();
        $data['billing_cycle_list'] = $billing_cycle_list;

        $currentdate = date('Y-m-d H:i:s');

        

        if($request->session()->has('cycle_id')) {
            $cycle_ID = $request->session()->get('cycle_id');
            $cycles_data = DB::table('ban')->where('cycle_id', '=', $cycle_ID);

        } else {
            $cycles = DB::table('cycle')->where('start_date', '<=', $currentdate)->where('end_date', '>=', $currentdate);

            if ($cycles->count() > 0) {
                $res = $cycles->get()->toArray();
                $cycle_id = $res[0]->id;
            }

            $cycles_data = DB::table('ban')->where('cycle_id', '=', $cycle_id)->limit(5);

        }





        if ($cycles_data->count() > 0) {
            $data['result_ban_line'] = $cycles_data->get()->toArray();
        } else {
            $data['result_ban_line'] = array();
        }

        return View::make('pages.banpage',$data);

    }

    /**
     * Get Ban Records by cycle ID
     *  
     */


    public function getBanCycleLines(Request $request,$cycle_id=0)
    {
        $this->checkLoggedin($request);
        // dd($data);


        

        $request->session()->put('cycle_id', $cycle_id);


        if($request->session()->has('cycle_id')) {
            $cycle_ID = $request->session()->get('cycle_id');

        } else {
            $cycle_ID = $cycle_id;
        }
          

        $data = array();
        $billing_cycle_list = DB::table('cycle')->get()->toArray();
        $data['billing_cycle_list'] = $billing_cycle_list;

        $cycles_data = DB::table('ban')->where('cycle_id', '=', $cycle_ID);
        
        if ($cycles_data->count() > 0) {
            $data['result_ban_line'] = $cycles_data->get()->toArray();
        } else {
            $data['result_ban_line'] = array();
        }

        return View::make('pages.banpage',$data);

    }

    /**
     * Get Ban Detail by Ban ID
     *  
     */


    public function getBanDetail(Request $request,$ban_id=0)
    {
        $this->checkLoggedin($request);

        $data = array();

        $billing_cycle_list = DB::table('cycle')->get()->toArray();
        $data['billing_cycle_list'] = $billing_cycle_list;

        $phone_number_res = DB::table('phone_number')
                                    ->where('ban_id', '=', $ban_id);
                                    // ->join('data_log', 'phone_number.id', '=', 'data_log.phone_number_id')
                                    // ->join('text_log', 'phone_number.id', '=', 'text_log.phone_number_id')
                                    // ->join('voice_log', 'phone_number.id', '=', 'voice_log.phone_number_id');
        
        $banRes = DB::table('ban')->where('id', '=', $ban_id)->first();
        if (!empty($banRes) ) {
            $data['ban_number'] =  $banRes->number;
        } 
        $phone_number_res->get()->toArray();

        $data_rated_amount_sum = 0;
        $data_sum = 0;
        $texts_sum = 0;
        $voice_min_sum = 0;
        $voice_rated_amount_sum = 0;
        $text_rated_amount_sum = 0;
        $voice_num_sum = 0;


        if ($phone_number_res->count() > 0) {
            $phone_numbers_res_data = $phone_number_res->get()->toArray();

            foreach ($phone_numbers_res_data as $value) {

                // $text_log_count = DB::table('text_log')->where('phone_number_id', '=', $value->phone_number_id)->orderBy('channelSeizureDate', 'desc');

                $data_rated_amount_sum += $value->data_rated_amount;
                $data_sum += $value->data;
                $texts_sum += $value->texts;
                $voice_min_sum += $value->voice_min;

                $voice_rated_amount_sum += $value->voice_rated_amount;
                $text_rated_amount_sum += $value->text_rated_amount;
                $voice_num_sum += $value->voice_num;
            }

            $data['data_rated_amount_sum'] = $data_rated_amount_sum;
            $data['data_sum'] = $data_sum;
            $data['texts_sum'] = $texts_sum;
            $data['voice_min_sum'] = $voice_min_sum;
            $data['voice_rated_amount_sum'] = $value->voice_rated_amount;
            $data['text_rated_amount_sum'] = $value->text_rated_amount;
            $data['voice_num_sum'] = $value->voice_num;

            $data['phone_numbers_list'] = $phone_numbers_res_data;

        } else {
            $data['phone_numbers_list'] = array();
        }

        return View::make('pages.phonenumberslist',$data);

    }

    /**
     * Get Phone Detail with All tables(data_log, text_log, voice_log ) Detail by phone_number_id ID
     *  
     */

    public function phoneDetail(Request $request,$phone_number_id =0 )
    {
        $this->checkLoggedin($request);

        $data = array();

        $billing_cycle_list = DB::table('cycle')->get()->toArray();
        $data['billing_cycle_list'] = $billing_cycle_list;

        $voice_log_num =0;
        $voice_log_ratedAmount = 0;
        $voice_log_callDuration = 0;
        $text_log_ratedAmount = 0;
        $phone_number_texts =0;
        $data_log_ratedAmount = 0;
        $data_log_callVolume = 0;

        $phone_number_res = DB::table('phone_number')->where('id', '=', $phone_number_id);
        $phone_number_res->get()->toArray();

        if ($phone_number_res->count() > 0) {
            $phone_numbers_res_data = $phone_number_res->get()->toArray();
            $data['phone_numbers_list'] = $phone_numbers_res_data;
            $phone_number  = $phone_numbers_res_data[0]->number;
            $phone_1 = substr($phone_number, 0, 3);
            $phone_2 = substr($phone_number, 3, 3);
            $phone_3 = substr($phone_number, 6, 4);

            $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3;
            $ban_id  = $phone_numbers_res_data[0]->ban_id;
        }
        $data['ban_id'] = $ban_id;
        $data['phone_number'] = $phone_number;

        
        $banRes = DB::table('ban')->where('id', '=', $ban_id)->first();
        if (!empty($banRes) ) {
            $data['ban_number'] =  $banRes->number;
        }

        // Phone Data_log Records
        $phone_numbers_list_data = DB::table('data_log')->where('phone_number_id', '=', $phone_number_id)->orderBy('channelSeizureDate', 'desc');

        if ($phone_numbers_list_data->count() > 0) {
            $phone_numbers_res_data = $phone_numbers_list_data->get()->toArray();
            $data['phone_numbers_list_data'] = $phone_numbers_res_data;
            foreach ($phone_numbers_res_data as $value) {
                $data_log_ratedAmount += $value->ratedAmount;
                $data_log_callVolume += $value->callVolume;
            }
        } else {
            $data['phone_numbers_list_data'] = array();
        }

        // Phone Text_log Records
        $phone_numbers_list_text = DB::table('text_log')->where('phone_number_id', '=', $phone_number_id)->orderBy('channelSeizureDate', 'desc');

        if ($phone_numbers_list_text->count() > 0) {
            $phone_numbers_res_text = $phone_numbers_list_text->get()->toArray();
            $data['phone_numbers_list_text'] = $phone_numbers_res_text;
            foreach ($phone_numbers_res_text as $value) {
                $text_log_ratedAmount += $value->ratedAmount;
                $phone_number_texts++;
            }
        } else {
            $data['phone_numbers_list_text'] = array();
        }


        // Phone Text_log Records
        $phone_numbers_list_voice = DB::table('voice_log')->where('phone_number_id', '=', $phone_number_id)->orderBy('channelSeizureDate', 'desc');

        if ($phone_numbers_list_voice->count() > 0) {
            $phone_numbers_res_voice = $phone_numbers_list_voice->get()->toArray();
            $data['phone_numbers_list_voice'] = $phone_numbers_res_voice;
            foreach ($phone_numbers_res_voice as $value) {
                $voice_log_num++;
                $voice_log_ratedAmount += $value->ratedAmount;
                $voice_log_callDuration += $value->callDuration;
            }
        } else {
            $data['phone_numbers_list_voice'] = array();
        }

        $data['voice_log_num'] = $voice_log_num;
        $data['voice_log_ratedAmount'] = $voice_log_ratedAmount;
        $data['voice_log_callDuration'] = $voice_log_callDuration;
        $data['text_log_ratedAmount'] = $text_log_ratedAmount;
        $data['phone_number_texts'] = $phone_number_texts;
        $data['data_log_ratedAmount'] = $data_log_ratedAmount;
        $data['data_log_callVolume'] = $data_log_callVolume;

        return View::make('pages.phone-number-detail',$data);

    }


    protected function checkLoggedin($request)
    {
        // print_r($request->session()->get('isloggedin'));
        if(!$request->session()->get('isloggedin')){
            redirect('pages.login');
            die();
        }
    }

}
