<?php
namespace App\Http\Controllers;
ini_set('max_execution_time', env('MAXIMUM_EXECUTION_TIME', 60));

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Model\PhoneNumber;
use App\Model\Cycle;
use App\Model\Ban;
use App\Model\CronLog;
use App\Model\DataLog;
use App\Model\VoiceLog;
use App\Model\TextLog;
use Carbon\Carbon;

use Exception;

class ApiCronController extends Controller
{

    protected function apiConnection($requestType, $requestTo)
    {
        try {
            $client     = new Client();
            $response   = $client->$requestType(config('app.goknowsAPI.api_url').$requestTo, [
                'headers' => [
                    'X-API-KEY'     => config('app.goknowsAPI.X-API-KEY'),
                    "Content-Type"  => "application/json",
                ]
            ]);
            return json_decode($response->getBody());
        } catch (Exception $e) {
            \Log::info($e->getCode());
            $message      = $e->getMessage();
            $errorMessage = substr($message, strpos($message, '"message'));
            $errorMessage = str_replace(['"message":', '"'], '', substr($errorMessage, 0, strpos($errorMessage, ',')));
            $message      = $errorMessage ?: $message;
            $this->insertLogs($message, 0);
        }
    }

    protected function insertPhoneBanData($banNumber, $phoneNumber, $cycleId)
    {
        $phone          =  PhoneNumber::where([
            ['number',   '=', $phoneNumber],
            ['cycle_id', '=', $cycleId]
        ]);
        $insertBan      = false;
        $insertPhone    = false;
        if (isset($phone->first()->id)) {
            $ban = Ban::find($phone->first()->ban_id);
            if ($ban->cycle_id != $phone->first()->cycle_id || !$ban) {
                $insertBan  = true;
            }
        } else {
            $insertPhone    = true;
            $insertBan      = true;
        }
        // Insert Data
        if ($insertBan) {
            $ban = Ban::create([
                'cycle_id' => $cycleId,
                'number'   => $banNumber
            ]);
        }

        if ($insertPhone) {
            $phone = PhoneNumber::create([
                'cycle_id' => $cycleId,
                'ban_id'   => $ban->id,
                'number'   => $phoneNumber
            ]);
        } else {
            $phone->update([
                'ban_id'   => $ban->id
            ]);
        }
    }

    protected function insertDataLogs($phoneNumber, $cycleId)
    {
        $usageDetails = $this->apiConnection('get', 'lines/'.$phoneNumber.'/usage_details');
        $phone        = PhoneNumber::where([
            ['number',   '=', $phoneNumber],
            ['cycle_id', '=', $cycleId]
        ]);
        $phone        = PhoneNumber::find($phone->first()->id);
        if (isset($phone->id) && $usageDetails) {
            $this->dataLogs($usageDetails->data, $phone);
            $this->voiceLogs($usageDetails->voice, $phone);
            $this->textLogs($usageDetails->text, $phone);
        }
    }

    protected function dataLogs($dataDetails, $phone)
    {
        foreach ($dataDetails as $data) {
            $alreadyUsed = $phone->dataLogs->where('channelSeizureDate', $data->channelSeizureDate);
            if (!$alreadyUsed->count()) {
                $bucketService = isset($data->bucket[0]->bucketService) ? $data->bucket[0]->bucketService : 0;
                $bucketFeature = isset($data->bucket[0]->bucketFeature) ? $data->bucket[0]->bucketFeature : 0;
                DataLog::create([
                    'phone_number_id'   => $phone->id,
                    'channelSeizureDate'=> $data->channelSeizureDate,
                    'callType'          => $data->callType,
                    'bucketService'     => $bucketService,
                    'bucketFeature'     => $bucketFeature,
                    'ratedAmount'       => $data->ratedAmount,
                    'callVolume'        => $data->callVolume,
                    'messageType'       => $data->messageType,
                    'roamingIndicator'  => $data->roamingIndicator,
                    'unitOfMeasure'     => $data->unitOfMeasure,
                    'usageCategory'     => $data->usageCategory,
                    'created_at'        => Carbon::now(),
                ]);
            }
        }
        // Get updated amounts for ban and phone_number tables.
        $logs   = DataLog::where('phone_number_id', $phone->id);
        $update = ['first' => ['data', $logs->sum('callVolume')], 'second' => ['data_rated_amount', $logs->sum('ratedAmount')]];
        $this->updateBanAndPhone($phone, $update);
    }

    protected function voiceLogs($dataDetails, $phone)
    {
        foreach ($dataDetails as $data) {
            $alreadyUsed = $phone->voiceLogs->where('channelSeizureDate', $data->channelSeizureDate);
            if (!$alreadyUsed->count()) {
                $bucketService = isset($data->bucket[0]->bucketService) ? $data->bucket[0]->bucketService : 0;
                $bucketFeature = isset($data->bucket[0]->bucketFeature) ? $data->bucket[0]->bucketFeature : 0;
                VoiceLog::create([
                    'phone_number_id'   => $phone->id,
                    'callDuration'      => $data->callDuration,
                    'callType'          => $data->callType,
                    'carrier'           => $data->carrier,
                    'channelSeizureDate'=> $data->channelSeizureDate,
                    'destinationCity'   => $data->destinationCity,
                    'destinationNumber' => $data->destinationNumber,
                    'destinationState'  => $data->destinationState,
                    'originCity'        => $data->originCity,
                    'originState'       => $data->originState,
                    'bucketService'     => $bucketService,
                    'bucketFeature'     => $bucketFeature,
                    'ratedAmount'       => $data->ratedAmount,
                    'billLegend'        => $data->billLegend,
                    'messageType'       => $data->messageType,
                    'direction'         => $data->direction,
                    'origin'            => $data->origin,
                    'roamingIndicator'  => $data->roamingIndicator,
                    'unitOfMeasure'     => $data->unitOfMeasure,
                    'usageCategory'     => $data->usageCategory,
                    'created_at'        => Carbon::now(),
                ]);
            }
        }
        // Get updated amounts for ban and phone_number tables.
        $logs   = VoiceLog::where('phone_number_id', $phone->id);
        $update = ['first' => ['texts', $logs->count()], 'second' => ['voice_rated_amount', $logs->sum('ratedAmount')]];
        $this->updateBanAndPhone($phone, $update);
    }

    protected function textLogs($dataDetails, $phone)
    {
        foreach ($dataDetails as $data) {
            $alreadyUsed = $phone->textLogs->where('channelSeizureDate', $data->channelSeizureDate);
            if (!$alreadyUsed->count()) {
                $bucketService = isset($data->bucket[0]->bucketService) ? $data->bucket[0]->bucketService : 0;
                $bucketFeature = isset($data->bucket[0]->bucketFeature) ? $data->bucket[0]->bucketFeature : 0;
                TextLog::create([
                    'phone_number_id'       => $phone->id,
                    'callType'              => $data->callType,
                    'channelSeizureDate'    => $data->channelSeizureDate,
                    'destinationCity'       => $data->destinationCity,
                    'destinationNumber'     => $data->destinationNumber,
                    'destinationState'      => $data->destinationState,
                    'direction'             => $data->direction,
                    'bucketService'         => $bucketService,
                    'bucketFeature'         => $bucketFeature,
                    'ratedAmount'           => $data->ratedAmount,
                    'messageType'           => $data->messageType,
                    'billLegend'            => $data->billLegend,
                    'origin'                => $data->origin,
                    'roamingIndicator'      => $data->roamingIndicator,
                    'unitOfMeasure'         => $data->unitOfMeasure,
                    'usageCategory'         => $data->usageCategory,
                    'created_at'            => Carbon::now(),
                ]);
            }
        }
        // Get updated amounts for ban and phone_number tables.
        $logs           = TextLog::where('phone_number_id', $phone->id);
        $textCount      = $phone->texts ?: 0;
        $update = ['first' => ['texts', $textCount + $logs->count()], 'second' => ['text_rated_amount', $logs->sum('ratedAmount')]];
        $this->updateBanAndPhone($phone, $update);
    }

    protected function updateBanAndPhone($phone, $update)
    {
        $phone->update([
            $update['first'][0]     => $update['first'][1],
            $update['second'][0]    => $update['second'][1]
        ]);
        $phone->banData->update([
            $update['first'][0]     => $update['first'][1],
            $update['second'][0]    => $update['second'][1]
        ]);
    }

    public function getAllLines()
    {
        $phoneNumbersData   = $this->apiConnection('get', 'lines');
        if ($phoneNumbersData) {
            $this->generateCycles();
            $cycleId = Cycle::where([
                ['start_date','<=', Carbon::now()],
                ['end_date', '>=', Carbon::now()]
            ])->first()->id;
            $startTime = Carbon::now();
            $count     = env('REQUESTS_PER_MINUTE');
            foreach ($phoneNumbersData as $key => $numberData) {
                try {            
                    $banNumber      = $numberData->ban;
                    $phoneNumber    = $numberData->phone_number;
                    $this->insertPhoneBanData($banNumber, $phoneNumber, $cycleId);
                    $this->insertDataLogs($phoneNumber, $cycleId);
                    if ($key + 1 == $count) {
                        $count = $count + env('REQUESTS_PER_MINUTE');
                        $endTime = Carbon::now();
                        $timeDiff = $endTime->diffInSeconds($startTime);
                        if ($timeDiff <= 59) {
                            sleep(60 - $timeDiff);
                        }
                        $startTime = Carbon::now();
                    }
                } catch (Exception $e) {
                    $this->insertLogs($e->getMessage(), 0);
                }
            }
            $this->insertLogs('', 1);
        }
    }

    protected function generateCycles()
    {
        $startDate  = Carbon::now()->format('Y/m/11');
        $endDate    = Carbon::parse($startDate)->addMonth()->format('Y/m/10');
        $totalCycles = Carbon::parse($startDate)->addYears(20)->diffInMonths($startDate);
        $maxDate    = Cycle::all()->max('end_date');
        if (!$maxDate || Carbon::now()->gt($maxDate)) {
            for ($i = 1; $i <= $totalCycles; $i++) {
                Cycle::create([
                    'start_date' => $startDate,
                    'end_date'   => $endDate
                ]);
                $startDate = Carbon::parse($startDate)->addMonth()->format('Y/m/11');
                $endDate   = Carbon::parse($startDate)->addMonth()->format('Y/m/10');
            }
        }
    }

    protected function insertLogs($message, $status)
    {
        CronLog::create([
            'run_date' => Carbon::now(),
            'success'  => $status,
            'error'    => $message
        ]);
    }

    public function getLogs()
    {
        $logs = CronLog::orderBy('run_date', 'desc')->paginate(10);
        return view('logs', compact('logs'));
    }

}
