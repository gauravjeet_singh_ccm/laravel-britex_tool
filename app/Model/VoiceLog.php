<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VoiceLog extends Model
{
    protected $table = 'voice_log';
    protected $fillable = [
        'phone_number_id', 'callDuration', 'callType',
        'carrier', 'channelSeizureDate', 'destinationCity',
        'destinationNumber', 'destinationState', 'originCity',
        'originState', 'bucketService', 'bucketFeature', 'ratedAmount',
        'billLegend', 'messageType', 'direction', 'origin', 'roamingIndicator',
        'unitOfMeasure', 'usageCategory', 'created_at'
    ];
    public $timestamps  = false;
}
