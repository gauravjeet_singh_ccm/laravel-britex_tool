<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TextLog extends Model
{
    protected $table = 'text_log';
    protected $fillable = [
        'phone_number_id', 'callType', 'channelSeizureDate', 'destinationCity',
        'destinationNumber', 'destinationState', 'direction', 'bucketService',
        'bucketFeature', 'ratedAmount', 'messageType', 'billLegend', 'origin',
        'roamingIndicator', 'unitOfMeasure', 'usageCategory', 'created_at'
    ];
    public $timestamps  = false;
}
