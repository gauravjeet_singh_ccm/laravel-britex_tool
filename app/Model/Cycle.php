<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cycle extends Model
{
    protected $table    = 'cycle';
    protected $fillable = ['start_date', 'end_date'];
    public $timestamps  = false;
}
