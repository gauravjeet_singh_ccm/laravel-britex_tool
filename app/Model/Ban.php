<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ban extends Model
{
    protected $table    = 'ban';
    protected $fillable = [
        'cycle_id', 'number', 'data_rated_amount', 
        'text_rated_amount', 'voice_rated_amount', 
        'data', 'texts', 'voice_num', 'voice_min'
    ];
    public $timestamps  = false;
}
