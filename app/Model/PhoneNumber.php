<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model
{
    protected $table    = 'phone_number';
    protected $fillable = [
        'cycle_id', 'ban_id', 'number', 'data_rated_amount',
        'text_rated_amount', 'voice_rated_amount', 'data',
        'texts', 'voice_num', 'voice_min'
    ];
    public $timestamps  = false;
    
    public function ban()
    {
        return $this->hasOne('App\Model\Ban', 'id', 'ban_id');
    }
    // Always call banData
    public function banData()
    {
        return $this->ban()->where('cycle_id', $this->cycle_id);
    }

    public function dataLogs()
    {
        return $this->hasMany('App\Model\DataLog', 'phone_number_id', 'id');
    }

    public function voiceLogs()
    {
        return $this->hasMany('App\Model\VoiceLog', 'phone_number_id', 'id');
    }

    public function textLogs()
    {
        return $this->hasMany('App\Model\TextLog', 'phone_number_id', 'id');
    }
}
