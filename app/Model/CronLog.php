<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CronLog extends Model
{
    protected $table = 'cron_log';
    protected $fillable = ['run_date', 'success', 'error'];
    public $timestamps = false;
}
