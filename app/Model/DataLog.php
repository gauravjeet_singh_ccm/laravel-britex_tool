<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataLog extends Model
{
    protected $table = 'data_log';
    protected $fillable = [
        'phone_number_id', 'channelSeizureDate', 'callType', 'bucketService',
        'bucketFeature', 'ratedAmount', 'callVolume', 'messageType', 'roamingIndicator',
        'unitOfMeasure', 'usageCategory', 'created_at'
    ];
    public $timestamps  = false;
}
