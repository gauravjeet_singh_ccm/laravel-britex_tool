<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('phone_number_id');
            $table->datetime('channelSeizureDate');
            $table->string('callType');
            $table->string('bucketService');
            $table->string('bucketFeature');
            $table->decimal('ratedAmount', 10, 4);
            $table->decimal('callVolume', 10, 4);
            $table->string('messageType');
            $table->string('roamingIndicator');
            $table->string('unitOfMeasure');
            $table->string('usageCategory');
            $table->datetime('created_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_log');

    }
}
