<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ban', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cycle_id');
            $table->bigInteger('number');
            $table->string('data_rated_amount')->nullable();
            $table->string('text_rated_amount')->nullable();
            $table->integer('voice_rated_amount')->nullable();
            $table->decimal('data',10,4)->nullable();
            $table->integer('texts')->nullable();
            $table->integer('voice_num')->nullable(); // phone_number.voice_num
            $table->decimal('voice_min',10,2)->nullable(); // phone_number.voice_min
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ban');
    }
}
