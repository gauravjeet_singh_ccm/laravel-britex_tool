<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TextLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text_log', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->integer('phone_number_id');
            $table->string('callType');
            $table->datetime('channelSeizureDate');
            $table->string('destinationCity');
            $table->string('destinationNumber');
            $table->string('destinationState');
            $table->string('direction');
            $table->string('bucketService');
            $table->string('bucketFeature');
            $table->decimal('ratedAmount',10,4);
            $table->string('messageType');
            $table->string('billLegend');
            $table->string('origin');
            $table->string('roamingIndicator');
            $table->string('unitOfMeasure');
            $table->string('usageCategory');
            $table->datetime('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_log');
    }
}
