<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhoneNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_number', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cycle_id');
            $table->integer('ban_id');
            $table->biginteger('number');
            $table->decimal('data_rated_amount',10,4)->nullable();
            $table->decimal('text_rated_amount',10,4)->nullable();
            $table->decimal('voice_rated_amount',10,4)->nullable();
            $table->decimal('data',10,4)->nullable();
            $table->integer('texts')->nullable();
            $table->integer('voice_num')->nullable();
            $table->decimal('voice_min',10,2)->nullable(); // Voice.callDuration

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone_number');

    }
}
