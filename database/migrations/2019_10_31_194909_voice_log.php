<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VoiceLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voice_log', function(Blueprint $table){

            $table->bigIncrements('id');
            $table->integer('phone_number_id');
            $table->integer('callDuration');
            $table->string('callType');
            $table->string('carrier');
            $table->datetime('channelSeizureDate');
            $table->string('destinationCity');
            $table->string('destinationNumber');
            $table->string('destinationState');
            $table->string('originCity');
            $table->string('originState');
            $table->string('bucketService');
            $table->string('bucketFeature');
            $table->decimal('ratedAmount',10,4);
            $table->string('billLegend');
            $table->string('messageType');
            $table->string('direction');
            $table->string('origin');
            $table->string('roamingIndicator');
            $table->string('unitOfMeasure');
            $table->string('usageCategory');
            $table->datetime('created_at')->useCurrent();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('voice_log');
    }
}
