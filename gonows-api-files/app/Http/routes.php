<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
	/* \Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
				echo'<pre>';
				var_dump($query->sql);
				var_dump($query->bindings);
				var_dump($query->time);
				echo'</pre>';
			}); 
			*/
		//echo phpinfo();	

Route::auth();
Route::get('/register','Auth\AuthController@notallowed');
Route::get('/password/reset', 'Auth\PasswordController@getEmail');
Route::post('/password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::get('/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('/password/reset', 'Auth\PasswordController@reset');
Route::get('conflict-script','ConflictScript@index');
Route::group(['middleware' => ['auth']], function() {
	Route::group(['middleware' => ['restrict']], function() {
		Route::get('/admin', 'Admin\AdminController@dashboard');
		Route::get('/admin/addusers', 'Admin\AdminController@addUsers');
		Route::post('/admin/addusers', 'Admin\AdminController@addUsers');
		Route::get('/admin/editusers/{id}', 'Admin\AdminController@editUsers');
		Route::post('/admin/editusers/{id}', 'Admin\AdminController@editUsers');
		Route::get('/admin/deleteuser/{id}', 'Admin\AdminController@delUsers');
	});
	Route::get('/', 'SearchController@searchForm');
	
	Route::get('inventory-snapshot', 'SearchController@inventorysnapshotForm');
	Route::post('inventory-snapshot','SearchController@inventorysnapshotForm');
	
	Route::get('ajax-get-models','SearchController@ajaxGetModels');
	Route::post('search-request','SearchController@searchRequest');
	Route::post('populate-request','SearchController@populatebyRequested');
	Route::get('filterdata','SearchController@filterdata');

	
	Route::get('reservation','ReservationController@makeReservation');
	Route::get('order/edit/{id?}','ReservationController@editOrder');
	Route::post('save-reservation','ReservationController@saveReservation');
	Route::post('update-reservation','ReservationController@updateReservation');
	Route::get('search-order','ReservationController@searchOrder');
	
	Route::post('update-order_status_edit_order','ReservationController@update_order_status_change');
	
	
	//tentative reservation
	Route::get('tentative-reservation','TentativeController@makeReservation');
	Route::post('save-tentative-reservation','TentativeController@saveReservation');
	Route::get('confirm-tentative-order','TentativeController@confirmOrder');
	//ends


	Route::get('checking-in','CheckingController@checkingIn');
	Route::get('checking-out','CheckingController@checkingOut');
	Route::any('update-item-totals','UpdateScript@index');

	Route::post('update-check-in-time','CheckingController@updateCheckInTime');
	Route::post('update-check-out-time','CheckingController@updateCheckOutTime');
	Route::post('ajax-scan-item','CheckingController@ajaxScanItem');
	Route::post('ajax-checkin-scan-item','CheckingController@ajaxCheckinScanItem');
	Route::post('cancel-checkout-scan-item','CheckingController@cancelCheckoutScanItem');
	Route::post('cancel-checkin-scan-item','CheckingController@cancelCheckinScanItem');
	
	//update order status on checkin/out
	Route::post('update-check_out_in-status','CheckingController@check_inout_update_order_status');

	Route::get('ajax-get-orders-by-location/{location}','CheckingController@ajaxGetOrdersByLocation');
	Route::get('ajax-get-orders-by-location-checkout/{location}','CheckingController@ajaxGetOrdersByLocationCheckout');
	Route::get('ajax-get-orders-by-location-checkin/{location}','CheckingController@ajaxGetOrdersByLocationCheckin');
	Route::get('ajax-get-checkout-order-detail/{orderId}','CheckingController@ajaxGetCheckoutOrderDetail');
	Route::get('ajax-get-checkin-order-detail/{orderId}','CheckingController@ajaxGetCheckinOrderDetail');
	
	//IMPORT ITEMS DATA
	Route::get('import','ImportController@index');
	Route::post('import-file','ImportController@import');
	Route::get('alert-script','alertScript@index');
	
	//EXPORT ITEMS DATA
	Route::get('export','ImportController@export_itmes');

	Route::get('calendar','CalendarController@calendar');
	Route::get('getcalendareventdata','CalendarController@dataCalendar');
	Route::get('orderbydate','CalendarController@getCalendardaydata');

	//TEST POPULATE CONTROLLERS
	Route::get('populate','PopulateController@makeReservation');

	Route::get('populate/ajax-get-models','PsearchController@ajaxGetModels');
	Route::post('populate/populate-request','PsearchController@populatebyRequested');
	Route::get('populate/filterdata','PsearchController@filterdata');

	Route::get('leads','LeadController@getIndex');
	Route::get('lead/notes/{id?}','LeadController@getNotes');
	Route::post('add_note','LeadController@postNote');
	//TEST ENDS
});