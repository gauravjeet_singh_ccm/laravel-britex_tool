$(document).ready(function(){
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });

    // $('#CsvReportForm').on("submit",function(e){
        // $('#CsvReportForm')[0].reset();

        // return false;

        // e.preventDefault();

        
        // var ajaxurl = $('#CsvReportForm').attr('action');

        // // return false;
        // $.ajax({
        //     type: 'POST',
        //     url: ajaxurl,
        //     data: $('#CsvReportForm').serialize(),
        //     success: function (data) {
        //         alert(data);
        //         // $("#msg").html(data.msg);
        //     }
        // });
    // });

     $(".reports_in_csv").on('change',function()
     {
        if ($(this).val() == 'all') {
            $('.specific_numbers_form_field, .specific_sims_form_field').hide();
            $('.specific_numbers_form_field textarea, .specific_sims_form_field textarea').text('');
            $('.specific_numbers_form_field textarea, .specific_sims_form_field textarea').prop('required',false);
            $('.specific_numbers_form_field textarea, .specific_sims_form_field textarea').attr('disabled');
        }
        if($(this).val() =='specific_numbers') 
        {
            $('.specific_numbers_form_field').show();
            $('.specific_numbers_form_field textarea').text('');
            $('.specific_numbers_form_field textarea').removeAttr('disabled');
            $(".specific_numbers_form_field textarea").prop('required', true);
            
            $('.specific_sims_form_field').hide();
            $('.specific_sims_form_field textarea').text('');
            $('.specific_sims_form_field textarea').attr('disabled');
            $(".specific_sims_form_field textarea").prop('required', false);
        }
        else if ($(this).val() == 'specific_sims')  {
            $('.specific_numbers_form_field').hide();
            $('.specific_numbers_form_field textarea').text('');
            $('.specific_numbers_form_field textarea').attr('disabled');
            $(".specific_numbers_form_field textarea").prop('required', false);

            $('.specific_sims_form_field').show();
            $('.specific_sims_form_field textarea').text('');
            $('.specific_sims_form_field textarea').removeAttr('disabled');
            $(".specific_sims_form_field textarea").prop('required', true);
        }

    })
    
});

