<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use App\Http\Requests;
use GuzzleHttp\Client;

// Main Index page Route
Route::get('/', function()
{
    $client = new Client();

    /*-------------- GET -> All Active Numbers  ---------------*/
    // Lines (mno=tmo, mno=att)
    $response = $client->request('GET', config('app.goknowsAPI.api_url').'lines/?mno=att', [
        'headers' => [
            'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),                
        ]
    ]);

    $result= $response->getBody();
    $data['result'] = json_decode($result);
    // dd($data);
    return View::make('pages.home',$data);
});

// restore single lines
Route::get('restore_line','ApiController@restore_line');

// numbers restore Bulk lines
Route::post('numberrestorelines','ApiController@restoreBulkLines');

// Restore Multiple lines one number per line
Route::post('restoreMultipleLines','ApiController@restoreMultipleLines');

// Suspend single line
Route::post('suspendline','ApiController@suspendline');

// Suspend bulk lines
Route::post('suspendBulkline','ApiController@suspendBulkline');

// Suspend Multiple Lines
Route::post('suspend_multiple_lines','ApiController@suspendMultipleLines');

// Swap Sims Changes bulk CSV
Route::post('bulk_swap_change_sims','ApiController@csvUploadBulkSwapChangeSims');

// Areacode Sims Changes bulk CSV
Route::post('csv_upload_bulk_areacode_change_sims','ApiController@csvUploadBulkAreacodeChangeSims');

// Active Lines bulk CSV
// Route::post('bulk_active_line_csv_upload','ApiController@bulkActivateLineCsvUpload');

// Export Lines from API in CSV file
Route::post('export_csv_files','ApiController@exportCsvFiles');
