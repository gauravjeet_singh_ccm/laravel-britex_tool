<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use App\Http\Requests;
use GuzzleHttp\Client;

// database access
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Model\PhoneNumber;
use App\Model\DataLog;
use App\Model\Cycle;
use App\Model\CronLog;

// Main Index page Route
Route::get('/', function(Request $request)
{
    if($request->session()->get('isloggedin')){

        // $client = new Client();

        // // /*-------------- GET -> All Active Numbers  ---------------*/
        // // Lines (mno=tmo, mno=att)
        // $response = $client->request('GET', config('app.goknowsAPI.api_url').'lines', [
        //     'headers' => [
        //         'X-API-KEY' => config('app.goknowsAPI.X-API-KEY'),                
        //     ]
        // ]);

        // $result= $response->getBody();
        // $data['result'] = json_decode($result);
        // // dd($data);
        // return View::make('pages.home',$data);
        // return view('homepage');
        $data=[];
        $billing_cycle_list = DB::table('cycle')->get()->toArray();
        $data['billing_cycle_list'] = $billing_cycle_list;

        return View::make('pages.home',$data);

    } else {
        return View::make('pages.login');
        // return view('pages.login');
    }

    
    

});

// restore single lines
Route::get('restore_line','ApiController@restore_line');

// numbers restore Bulk lines
Route::post('numberrestorelines','ApiController@restoreBulkLines');

// Restore Multiple lines one number per line
Route::post('restoreMultipleLines','ApiController@restoreMultipleLines');

// Suspend single line
Route::post('suspendline','ApiController@suspendline');

// Suspend bulk lines
Route::post('suspendBulkline','ApiController@suspendBulkline');

// Suspend Multiple Lines
Route::post('suspend_multiple_lines','ApiController@suspendMultipleLines');

// Swap Sims Changes bulk CSV
Route::post('bulk_swap_change_sims','ApiController@csvUploadBulkSwapChangeSims');

// Areacode Sims Changes bulk CSV
Route::post('csv_upload_bulk_areacode_change_sims','ApiController@csvUploadBulkAreacodeChangeSims');

// Active Lines bulk CSV
// Route::post('bulk_active_line_csv_upload','ApiController@bulkActivateLineCsvUpload');

// Export Lines from API in CSV file
Route::post('export_csv_files','ApiController@exportCsvFiles');

// Execute CronJob Callback Function
// Route::get('cronJobExecute','ApiController@cronJobExecute');
Route::get('cronJobExecute','CronJobController@cronJobExecute')->name('cronJobExecute');


Route::get('addCycles','CronJobController@addCycles')->name('addCycles');

Route::get('getBanLatestLines','CronJobController@getBanLatestLines')->name('getBanLatestLines');

Route::get('getBanCycleLines/{cycleid}','CronJobController@getBanCycleLines')->name('getBanCycleLines');

Route::get('banDetail/{benid}','CronJobController@getBanDetail');

Route::get('phoneDetail/{phoneid}','CronJobController@phoneDetail');

// Route::get('session/get','SessionController@accessSessionData');

Route::post('/loggin','SessionController@storeSessionData');

Route::get('/logout','SessionController@deleteSessionData');

Route::get('/get-lines', 'ApiCronController@getAllLines');

Route::get('/logs', 'ApiCronController@getLogs');

Route::get('/test', function() {
    $logs = CronLog::orderBy('run_date', 'desc')->limit(2)->get();
    echo '<h1>Logs</h1><hr>';
    foreach ($logs as $l) {
        if ($l->error) {
            echo '<br>';
            echo $l->run_date. ' -- ';
            echo $l->error;
            echo '<br><br>';
        } else if ($l->success) {
            echo '<br>';
            echo 'Cron ran successfully';
            echo '<br>';
        }
    }
});

