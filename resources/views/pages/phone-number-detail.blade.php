@extends('layouts.default')

@section('content')

	@if(Session::get('isloggedin'))

			{{-- Render Response of Multiple Restore Request --}}

			<div class="phone_numbers_list">

				@if(isset($phone_numbers_list))

					@if (count($phone_numbers_list) ==0)
						<div class="alert alert-danger">
							<h4>No Records Found!</h4>
						</div>
					@else

						<h3 class="table-padding"><a href="{{ url("/getBanLatestLines") }}">BAN Overview</a> > BAN <a href="{{ url("/banDetail/{$ban_id}") }}">#@if(isset($ban_number)){{ $ban_number }} @endif </a> > Phone @if(isset($phone_number)) {{ $phone_number }} @endif </h3>

						@if(isset($phone_numbers_list_data))

							@if(count($phone_numbers_list_data) == 0)
								<div class="alert alert-danger">
									<h4>No Data Records Found!</h4>
								</div>
							@else 

								<h3 class="table-padding">Data</h3>
								<div class="alert alert-info datalisting">
									<table class="table table-striped">
										<tr>
											<th>channelSeizureDate</th>
											<th>callType</th>
											<th>bucketService</th>
											<th>bucketFeature</th>
											<th>ratedAmount</th>
											<th>callVolume</th>
											<th>messageType</th>
											<th>roamingIndicator</th>
											<th>unitOfMeasure</th>
											<th>unitOfMeasure</th>
											<th>Created Time</th>

										</tr>

										@php( $i=0)
										
										@foreach ($phone_numbers_list_data as $item)

											{{-- {{  print_r($item) }}    --}}
											<tr>
												<td>@if(isset($item->channelSeizureDate)) {{ $item->channelSeizureDate }} @endif </td>
												<td>@if(isset($item->callType)) {{ $item->callType }} @endif </td>
												<td>@if(isset($item->bucketService)) {{ $item->bucketService }} @endif </td>
												<td>@if(isset($item->bucketFeature)) {{ $item->bucketFeature }} @endif </td>
												<td>@if(isset($item->ratedAmount)) {{ $item->ratedAmount }} @endif </td>
												<td>@if(isset($item->callVolume)) {{ $item->callVolume }} @endif </td>
												<td>@if(isset($item->messageType)) {{ $item->messageType }} @endif </td>
												<td>@if(isset($item->roamingIndicator)) {{ $item->roamingIndicator }} @endif </td>
												<td>@if(isset($item->unitOfMeasure)) {{ $item->unitOfMeasure }} @endif </td>
												<td>@if(isset($item->unitOfMeasure)) {{ $item->unitOfMeasure }} @endif </td>
												<td>@if(isset($item->created_at)) {{ $item->created_at }} @endif </td>
											</tr>
											@php($i++)

										@endforeach
										<tr>
											<th>TOTAL</th>
											<th colspan="3"></th>
											<th>@if(isset($data_log_ratedAmount)) {{ $data_log_ratedAmount }} @endif </th>
											<th>@if(isset($data_log_callVolume)) {{ $data_log_callVolume }} @endif </th>
											<th colspan="5"></th>
										</tr>
									</table>
								</div>
							@endif

						@endif

						@if(isset($phone_numbers_list_text))

							@if(count($phone_numbers_list_text) == 0)
								<div class="alert alert-danger">
									<h4>No Text Records Found!</h4>
								</div>
							@else 

								<h3 class="table-padding">Text</h3>
								<div class="alert alert-info datalisting">
									<table class="table table-striped">
										<tr>
											<th>callType</th>
											<th>channelSeizureDate</th>
											<th>destinationCity</th>
											<th>destinationNumber</th>
											<th>destinationState</th>
											<th>direction</th>
											<th>bucketService</th>
											<th>bucketFeature</th>
											<th>ratedAmount</th>
											<th>messageType</th>
											<th>billLegend</th>
											<th>origin</th>
											<th>roamingIndicator</th>
											<th>unitOfMeasure</th>
											<th>unitOfMeasure</th>
											<th>Created Time</th>
										</tr>

										@php( $i=0)
										
										@foreach ($phone_numbers_list_text as $item)

											{{-- {{  print_r($item) }}    --}}
											<tr>
												<td>@if(isset($item->callType)) {{ $item->callType }} @endif </td>
												<td>@if(isset($item->channelSeizureDate)) {{ $item->channelSeizureDate }} @endif </td>
												<td>@if(isset($item->destinationCity)) {{ $item->destinationCity }} @endif </td>
												<td>@if(isset($item->destinationNumber)) {{ $item->destinationNumber }} @endif </td>
												<td>@if(isset($item->destinationState)) {{ $item->destinationState }} @endif </td>
												<td>@if(isset($item->direction)) {{ $item->direction }} @endif </td>
												<td>@if(isset($item->bucketService)) {{ $item->bucketService }} @endif </td>
												<td>@if(isset($item->bucketFeature)) {{ $item->bucketFeature }} @endif </td>
												<td>@if(isset($item->ratedAmount)) {{ $item->ratedAmount }} @endif </td>
												<td>@if(isset($item->messageType)) {{ $item->messageType }} @endif </td>
												<td>@if(isset($item->billLegend)) {{ $item->billLegend }} @endif </td>
												<td>@if(isset($item->origin)) {{ $item->origin }} @endif </td>
												<td>@if(isset($item->roamingIndicator)) {{ $item->roamingIndicator }} @endif </td>
												<td>@if(isset($item->unitOfMeasure)) {{ $item->unitOfMeasure }} @endif </td>
												<td>@if(isset($item->unitOfMeasure)) {{ $item->unitOfMeasure }} @endif </td>
												<td>@if(isset($item->created_at)) {{ $item->created_at }} @endif </td>
											</tr>
											@php($i++)

										@endforeach
										<tr>
											<th>TOTAL</th>
											<th colspan="7"></th>
											<th>@if(isset($text_log_ratedAmount)) {{ $text_log_ratedAmount }} @endif </th>
											<th colspan="7"></th>
										</tr>
									</table>
								</div>
							@endif
						@endif

						@if(isset($phone_numbers_list_voice))
							@if(count($phone_numbers_list_voice) == 0)
								<div class="alert alert-danger">
									<h4>No Voice Records Found!</h4>
								</div>
							@else 

								<h3 class="table-padding">Voice</h3>
								
								<div class="alert alert-info datalisting">
									<table class="table table-striped">
										<tr>
											<th>callDuration</th>
											<th>callType</th>
											<th>carrier</th>
											<th>channelSeizureDate</th>
											<th>destinationCity</th>
											<th>destinationNumber</th>
											<th>destinationState</th>
											<th>originCity</th>
											<th>originState</th>
											<th>bucketService</th>
											<th>bucketFeature</th>
											<th>ratedAmount</th>
											<th>billLegend</th>
											<th>messageType</th>
											<th>direction</th>
											<th>origin</th>
											<th>roamingIndicator</th>
											<th>unitOfMeasure</th>
											<th>unitOfMeasure</th>
											<th>Created Time</th>

										</tr>

										@php( $i=0)
										
										@foreach ($phone_numbers_list_voice as $item)

											{{-- {{  print_r($item) }}    --}}
											<tr>
												<td>@if(isset($item->callDuration)) {{ $item->callDuration }} @endif </td>
												<td>@if(isset($item->callType)) {{ $item->callType }} @endif </td>
												<td>@if(isset($item->carrier)) {{ $item->carrier }} @endif </td>
												<td>@if(isset($item->channelSeizureDate)) {{ $item->channelSeizureDate }} @endif </td>
												<td>@if(isset($item->destinationCity)) {{ $item->destinationCity }} @endif </td>
												<td>@if(isset($item->destinationNumber)) {{ $item->destinationNumber }} @endif </td>
												<td>@if(isset($item->destinationState)) {{ $item->destinationState }} @endif </td>
												<td>@if(isset($item->originCity)) {{ $item->originCity }} @endif </td>
												<td>@if(isset($item->originState)) {{ $item->originState }} @endif </td>
												<td>@if(isset($item->bucketService)) {{ $item->bucketService }} @endif </td>
												<td>@if(isset($item->bucketFeature)) {{ $item->bucketFeature }} @endif </td>
												<td>@if(isset($item->ratedAmount)) {{ $item->ratedAmount }} @endif </td>
												<td>@if(isset($item->billLegend)) {{ $item->billLegend }} @endif </td>
												<td>@if(isset($item->messageType)) {{ $item->messageType }} @endif </td>
												<td>@if(isset($item->direction)) {{ $item->direction }} @endif </td>
												<td>@if(isset($item->origin)) {{ $item->origin }} @endif </td>
												<td>@if(isset($item->roamingIndicator)) {{ $item->roamingIndicator }} @endif </td>
												<td>@if(isset($item->unitOfMeasure)) {{ $item->unitOfMeasure }} @endif </td>
												<td>@if(isset($item->unitOfMeasure)) {{ $item->unitOfMeasure }} @endif </td>
												<td>@if(isset($item->created_at)) {{ $item->created_at }} @endif </td>

											</tr>
											@php($i++)

										@endforeach
										<tr><th colspan="19">TOTAL</th></tr>
										<tr>
											<th>@if(isset($voice_log_callDuration)) {{ $voice_log_callDuration }} @endif </th>
											<th colspan="10"></th> 
											<th>@if(isset($voice_log_ratedAmount)) {{ $voice_log_ratedAmount }} @endif </th>
											<th colspan="8"></th> 
										</tr>
									
									</table>
								</div>
							@endif
						@endif
					@endif

				@endif
			</div>

		</div>
		
	@endif

@stop