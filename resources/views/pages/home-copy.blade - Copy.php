@extends('layouts.default')

@section('content')

	<div class="container">
		<div class="account">
				<div class="table-padding">
					<h3>All Records</h3>
				</div>
		</div>
	
		<div class="tables all-records">

			@if(isset($result))
				@if (count($result) ==0)
					<div class="alert alert-danger">
						<h4>No Records Found!</h4>
					</div>
				@else	
					<div class="alert alert-success">
						<table class="table">
							<tr>
								<th>Phone Number</th>
								<th>Sim Number</th>
								<th>BAN</th>
								<th>Status</th>
							</tr>
							@foreach ($result as $item)

								@if(isset($item->phone_number))
									@php ($phone_1 = isset($item->phone_number) ? substr($item->phone_number, 0, 3) : '')
									@php ($phone_2 = isset($item->phone_number) ? substr($item->phone_number, 3, 3) : '')
									@php ($phone_3 = isset($item->phone_number) ? substr($item->phone_number, 6, 4) : '')
									@php ($phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3)
								@else
									@php ($phone_number = '')
								@endif									
								<tr>
								<td>{{ $phone_number }} </td>
								<td>{{ $item->sim_number }} </td>
								<td>{{ $item->ban }} </td>
								<td>{{ $item->status }} </td>
								</tr>
							@endforeach
						</table>

						{{--  print_r($result);  --}}
					</div>
				@endif
			@endif
		</div>
	</div>

	{{-- Restore Multiple LInes section --}}
	<div class="container">
		<hr>
		<div class="table-padding">
			<h3>Restore Multiple lines</h3>
		</div>
		<div class="Restore_line_form">
			
			<form method="POST" action="{{ url('restoreMultipleLines') }}">
				<div class="form-group">
					<textarea class="form-control" name="numbers_restore_lines" rows="5" cols="50" placeholder="1 number per line, eg: 123-456-7891" required >{{ old('numbers_restore_lines') }}</textarea>
					@if ( isset($request) AND $request->session()->exists('numbers_restore_lines'))
						@php $request->session()->flush(); @ndphp
					@endif
				</div>
				<div class="form-group">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<p> <input class="btn btn-primary"  type="submit" name="submit" value="Submit"></p>
				</div>

			</form>
		</div>

		{{-- Render Response of Multiple Restore Request --}}

		<div class="result_restore_line">

			@if(isset($result_restore_line))
				@if (count($result_restore_line) ==0)
					<div class="alert alert-danger">
						<h4>No Records Found!</h4>
					</div>
				@else	

					<div class="alert alert-info">

						<h3>Restore Multiple Lines Response</h3>
						
						{{-- <pre> --}}
						{{-- {{  dd($result_restore_line) }}	 --}}
						{{-- @php(print_r($result_restore_line)) --}}
						{{-- </pre> --}}
						
						<table class="table table-striped">
							<tr>
								<th width="130px">Number</th>
								<th>Title</th>
								<th>Message</th>
								<th>Error</th>
								<th>Code</th>
								<th>Status</th>
							</tr>

							@php( $i=0)
							
							@foreach ($result_restore_line as $item)

								{{-- {{  print_r($item) }}    --}}

								<tr>
								<td>{{ $number[$i] }} </td>
								<td>{{ $item->title }} </td>
								<td>{{ $item->message }} </td>
								<td>{{ $item->error }} </td>
								<td>{{ $item->code }} </td>
								<td>{{ $item->status }} </td>

								<tr>

								@php($i++)

							@endforeach
						</table>
					</div>
				@endif

				@endif
		</div>

	</div>

	{{-- Suspend Multiple LInes section --}}
	<div class="container">
		<hr>
		<div class="table-padding">
			<h3>Suspend Multiple lines</h3>
		</div>

		<div class="suspendmultiplelines">
			<form action="{{ url('suspend_multiple_lines') }}" method="POST" >
				<div class="form-group">
					<textarea name="numbers_suspend_lines" class="form-control" rows="5" cols="50" placeholder="1 number per line, eg: 123-456-7891 " required>{{ old('result_suspend_lines') }}</textarea>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					@if (isset($request) AND $request->session()->exists('result_suspend_lines'))
						@php $request->session()->flush(); @ndphp
					@endif
				</div>
				<div class="form-group">
					<p> <input type="submit" name="submit" value="Submit" class="btn btn-primary"></p>
				</div>

			</form>
		</div>
		
		{{-- Render Response of Multiple Restore Request --}}

		<div class="result_suspend_lines">

			@if(isset($result_suspend_lines))

				<div class="alert alert-warning">

					<h3>Suspend Multiple Lines Response</h3>
					
					<table class="table table-striped">
						<tr>
							<th width="130px">Number</th>
							<th>Title</th>
							<th>Message</th>
							<th>Error</th>
							<th>Code</th>
							<th>Status</th>
						</tr>

						@php( $i=0)
						
						@foreach ($result_suspend_lines as $item)

							{{-- {{  print_r($item) }}    --}}
							<tr>
							<td>{{ $number[$i] }} </td>
							<td>{{ $item->title }} </td>
							<td>{{ $item->message }} </td>
							<td>{{ $item->error }} </td>
							<td>{{ $item->code }} </td>
							<td>{{ $item->status }} </td>
							<tr>
							@php($i++)

						@endforeach
					</table>
				</div>

				@endif
		</div>

	</div>

	{{-- Sims Changes LInes section --}}
	<div class="container">
		
		<hr>
		<div class="table-padding">
			<h3>SWAP SIM Changes (Upload a CSV file to Change Multiple SIMS)</h3>
		</div>

		<form action="{{ url('bulk_swap_change_sims') }}" method="POST" enctype="multipart/form-data" >
			<div class="form-group">
				<input type="file" name="csv_upload" id="bulk_swap_change_sims" required>
				<label for="bulk_swap_change_sims" class="form-label">Uplaod CSV file to Assign Sim Number (This endpoint assigns a new sim number to an activated line.)</label>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" >
			</div>
			<div class="form-group">
				<p> <input type="submit" name="submit" value="Submit" class="btn btn-primary"></p>
			</div>

		</form>

		<div class="result_swap_sims">

			@if(isset($result_swap_sims))
				
				{{-- {{  "<pre>" }}
				{{  print_r($result_swap_sims) }}
				{{ "</pre>" }} --}}

				<h3>SWAP Sim Changes Response</h3>

				
				<table class="table table-striped">
					<tr>
						<th width="130px">Number</th>
						<th>Title</th>
						<th>Message</th>
						<th>Error</th>
						<th>Code</th>
						<th>Status</th>
					</tr>
					@php($i=0)
					@foreach ($result_swap_sims as $item)

						{{-- @php(print_r($item)) --}}
						<tr>
							<td>{{ $number[$i] }} </td>
							<td>{{ $item->title }} </td>
							<td>{{ $item->message }} </td>
							<td>{{ $item->error }} </td>
							<td>{{ $item->code }} </td>
							<td>{{ $item->status }} </td>
						<tr>
						@php($i++)
					
					@endforeach
				</table>

				@endif
		</div>

	</div>

	{{-- Areacode Changes LInes section --}}
	<div class="container">
		
		<hr>
		<div class="table-padding">
			<h3>Areacode Changes</h3>
		</div>

		<form action="{{ url('csv_upload_bulk_areacode_change_sims') }}" method="POST" enctype="multipart/form-data" >

			<div class="form-group">
				<input type="file" name="csv_upload_areacode" id="csv_upload_areacode" required>
				<label class="form-label" for="csv_upload_areacode">Upload CSV File to change Area code of SIMS (This endpoint assigns a new phone numbers to an activated lines.)</label>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" >
			</div>
			<div class="form-group">
				<p> <input type="submit" name="submit" value="Submit" class="btn btn-primary"></p>
			</div>
		</form>
		
		<div class="result_areacode_change">

			@if(isset($result_areacode_change))
				
				{{-- {{  "<pre>" }}
				{{  print_r($result_areacode_change) }}
				{{ "</pre>" }} --}}
				<div class="alert alert-info">
				<h3>Area Code Change Response</h3>

				
					<table class="table table-brdered">
						<tr>
							<th>Old Number</th>
							<th>New Phone Number</th>
							<th>Sim Number</th>
							<th>BAN</th>
							<th>Message (error)</th>
							<th>Status</th>
						</tr>
						@php($i=0)
						@foreach ($result_areacode_change as $item)
							{{-- @php(print_r($item)) --}}

							@if(isset($item->phone_number))
								@php ($phone_1 = isset($item->phone_number) ? substr($item->phone_number, 0, 3) : '')
								@php ($phone_2 = isset($item->phone_number) ? substr($item->phone_number, 3, 3) : '')
								@php ($phone_3 = isset($item->phone_number) ? substr($item->phone_number, 6, 4) : '')
								@php ($phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3)
							@else
								@php ($phone_number = '')
							@endif

							<tr class="alert @if(isset($item->phone_number)) alert-success @else alert-danger @endif">
								<td>@if($number[$i]){{ $number[$i] }} @endif </td>
								<td>@if(isset($phone_number)){{ $phone_number }} @endif </td>
								<td>@if(isset($item->sim_number) ){{ $item->sim_number }} @endif </td>
								<td>@if(isset($item->ban) ){{ $item->ban }} @endif </td>
								<td>@if(isset($item->message) ){{ $item->message }} ({{ $item->error }}) @endif </td>
								<td>@if(isset($item->status) ){{ $item->status }} @endif </td>
							<tr>
							@php($i++)
						@endforeach
					</table>
				</div>

				@endif
		</div>

	</div>

	{{-- Bulk Activate Lines via CSV File Section --}}
	<div class="container" style="display:none;">
		
		<hr>
		<div class="table-padding">
			<h3>Bulk Activate Lines(Upload CSV file)</h3>
		</div>

		<form action="{{ url('bulk_active_line_csv_upload') }}" method="POST" enctype="multipart/form-data" >

			<div class="form-group">
				<input type="file" name="bulk_active_line_csv" id="bulk_active_line_csv" required>
				<label class="form-label" for="bulk_active_line_csv">Upload CSV File to Activate Lines (This endpoint activates line in your account)</label>

				<input type="hidden" name="_token" value="{{ csrf_token() }}" >
			</div>
			<div class="form-group">
				<p> <input type="submit" name="submit" value="Submit" class="btn btn-primary"></p>
			</div>
		</form>
		
		<div class="result_activate_lines">

			@if(isset($result_activate_lines))
				
				{{--  {{  "<pre>" }}
				{{  print_r($result_activate_lines) }}
				{{ "</pre>" }}  --}}
				
				<table class="table table-striped">
					<tr>
						<th>SIM Number</th>
						<th>Title</th>
						<th>Message</th>
						<th>Error</th>
						<th>Code</th>
						<th>Status</th>
					</tr>
					@php($i=0)
					@foreach ($result_activate_lines as $item)
						{{-- @php(print_r($item)) --}}
						<tr>
							<td>{{ $sim_number[$i] }} </td>
							<td>{{ $item->title }} </td>
							<td>{{ $item->message }} </td>
							<td>{{ $item->error }} </td>
							<td>{{ $item->code }} </td>
							<td>{{ $item->status }} </td>
						<tr>
						@php($i++)
						

					@endforeach
				</table>
				

			@endif
		</div>

	</div>

	{{-- Export Reports in CSV File --}}
	<div class="container">
		
		<hr>
		<div class="table-padding">
			<h3>Export Reports in CSV</h3>
		</div>

		<div class="result_activate_lines">

			<form action="{{ url('export_csv_files') }}" method="POST" id="CsvReportForm" >

				<div class="form-group">
					<select name="reports_in_csv" class="form-control reports_in_csv" required>
						<option value="">Please Select Report type</option>
						<option value="all">All Phone Numbers</option>
						<option value="specific_numbers">Specific Phone Numbers</option>
						<option value="specific_sims">Specific Sims</option>
					</select>

					<input type="hidden" class="csrf_token" name="_token" value="{{ csrf_token() }}" >

				</div>
				
				<div class="form-group specific_numbers_form_field" >
					<textarea name="specific_numbers" class="form-control" rows="8" placeholder="Specific Numbers 1 per line, eg: 123-456-7891"></textarea>
				</div>
				<div class="form-group specific_sims_form_field" >
					<textarea name="specific_sims" class="form-control" rows="8" placeholder="Specific Sims 1 per line"></textarea>
				</div>
				<div class="form-group">
					<p> <input type="submit" name="submit" value="Submit" class="btn btn-primary"></p>
				</div>
			</form>

		</div>

	</div>
                        

@stop