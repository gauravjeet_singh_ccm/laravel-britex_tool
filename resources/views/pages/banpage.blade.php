@extends('layouts.default')

@section('content')

	@if(Session::get('isloggedin'))
	
			{{-- Render Response of Multiple Restore Request --}}

			<div class="result_ban_line">

				@if(isset($result_ban_line))
					@if (count($result_ban_line) ==0)
						<div class="alert alert-danger">
							<h4>No Records Found!</h4>
						</div>
					@else	

						<div class="alert alert-info">
							<h3 class="table-padding">Dashboard / BAN Overview</h3>
							<table class="table table-striped">
								<tr>
									<th width="130px">BAN</th>
									<th>Data Rated Amount ($)</th>
									<th>Text Rated Amount</th>
									<th>Voice Rated Amount</th>
									<th>Data (MB)</th>
									<th>Texts (number)</th>
									<th>Voice Num</th>
									<th>Voice (Minutes)</th>
								</tr>

								@php( $i=0)
								
								@foreach ($result_ban_line as $item)

									{{-- {{  print_r($item) }}    --}}
									<tr>
										<td>@if(isset($item->number)) <a href="{{ url("/banDetail/{$item->id}") }}">{{ $item->number }}</a>  @endif </td>
										<td>@if(isset($item->data_rated_amount)) {{ $item->data_rated_amount }} @endif </td>
										<td>@if(isset($item->text_rated_amount)) {{ $item->text_rated_amount }} @endif </td>
										<td>@if(isset($item->voice_rated_amount)) {{ $item->voice_rated_amount }} @endif </td>
										<td>@if(isset($item->data)) {{ $item->data }} @endif </td>
										<td>@if(isset($item->texts)) {{ $item->texts }} @endif </td>
										<td>@if(isset($item->voice_num)) {{ $item->voice_num }} @endif </td>
										<td>@if(isset($item->voice_min)) {{ $item->voice_min }} @endif </td>
										

									</tr>

									@php($i++)

								@endforeach
							</table>
						</div>
					@endif

				@endif
			</div>

		</div>
		
	@endif

@stop