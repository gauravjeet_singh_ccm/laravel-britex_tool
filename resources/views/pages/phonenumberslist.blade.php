@extends('layouts.default')

@section('content')


	@if(Session::get('isloggedin'))

		{{-- Render Response of Multiple Restore Request --}}

		<div class="phone_numbers_list">

			@if(isset($phone_numbers_list))

				@if (count($phone_numbers_list) ==0)
					<div class="alert alert-danger">
						<h4>No Records Found!</h4>
					</div>
				@else	

					<div class="alert alert-info">
						<h3 class="table-padding"><a href="{{ url("/getBanLatestLines") }}">BAN Overview</a> > BAN#@if(isset($ban_number)) {{ $ban_number }} @endif </h3>
						<table class="table table-striped">
							<tr>
								<th width="180px">Phone Number</th>
								<th>Data Rate Amount ($)</th>
								<th>Text Rated Amount</th>
								<th>Voice Rated Amount</th>
								<th>Data (MB)</th>
								<th>Texts (number)</th>
								<th>Voice Num</th>
								<th>Voice (Minutes)</th>
							</tr>

							@php( $i=0)
							
							@foreach ($phone_numbers_list as $item)

								{{-- {{  print_r($item) }}    --}}
								@php( $phone_1 = substr($item->number, 0, 3))
								@php( $phone_2 = substr($item->number, 3, 3))
								@php( $phone_3 = substr($item->number, 6, 4))
								@php( $phone_number = $phone_1.'-'.$phone_2.'-'.$phone_3)

								<tr>
									<td>@if(isset($item->number)) <a href="{{ url("/phoneDetail/{$item->id}") }}">{{ $phone_number }}</a> @endif </td>
									<td>@if(isset($item->data_rated_amount)) {{ $item->data_rated_amount }} @endif </td>
									<td>@if(isset($item->text_rated_amount)) {{ $item->text_rated_amount }} @endif </td>
									<td>@if(isset($item->voice_rated_amount)) {{ $item->voice_rated_amount }} @endif </td>
									<td>@if(isset($item->data)) {{ $item->data }} @endif </td>
									<td>@if(isset($item->texts)) {{ $item->texts }} @endif </td>
									<td>@if(isset($item->voice_num)) {{ $item->voice_num }} @endif </td>
									<td>@if(isset($item->voice_min)) {{ $item->voice_min }} @endif </td>
								</tr>
								@php($i++)

							@endforeach
							<tr>
								<th>TOTAL</th>
								<th>@if(isset($data_rated_amount_sum)) {{ $data_rated_amount_sum }} @endif </th>
								<th>@if(isset($text_rated_amount_sum)) {{ $text_rated_amount_sum }} @endif </th>
								<th>@if(isset($voice_rated_amount_sum)) {{ $voice_rated_amount_sum }} @endif </th>
								<th>@if(isset($data_sum)) {{ $data_sum }} @endif </th>
								<th>@if(isset($texts_sum)) {{ $texts_sum }} @endif </th>
								<th>@if(isset($voice_num_sum)) {{ $voice_num_sum }} @endif </th>
								<th>@if(isset($voice_min_sum)) {{ $voice_min_sum }} @endif </th>
							</tr>
						</table>
					</div>
				@endif

			@endif
		</div>

	@endif

@stop