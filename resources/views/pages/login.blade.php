@extends('layouts.default')

@section('content')

	@if(!Session::get('isloggedin'))
		<div class="login-form-container">

			<!-- LOGIN FORM -->
			<div class="text-center loginform well ">
				<h2 class="login-form-heading" >Login Form</h2>
				

				<!-- Main Form -->
				<div class="login-form-1 px-3" >
					@if(isset($error) )
						<div class="alert alert-danger " style="margin-top: 20px;margin-bottom: 20px;">{{ $error }}</div>
					@endif
					<form id="login-form" action="{{ url('loggin') }}" class="text-left" method="post">
						<div class="login-form-main-message"></div>
						<div class="main-login-form">
							<div class="login-group">
								<div class="form-group">
									<label for="lg_username" class="sr-only">Username</label>
									<input type="text" value="{{ old('username') }}" required autofocus class="form-control" id="lg_username" name="lg_username" required="" placeholder="username *">
									@if ($errors->has('email'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
	                                @endif
								</div>
								<div class="form-group">
									<label for="lg_password" class="sr-only">Password</label>
									<input type="password" class="form-control" required="" id="lg_password" name="lg_password" placeholder="password *">
								</div>
								<div class="form-group login-group-checkbox">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									
								</div>
							</div>
							<button type="submit" class="login-button btn btn-primary">LOGIN</button>
						</div>
						
					</form>
				</div>
				<!-- end:Main Form -->
			</div>
		</div>
    @endif

@stop