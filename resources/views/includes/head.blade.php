<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>GoKnows T-Mobile API</title>
<meta name="description" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
 
<!-- load bootstrap from a cdn -->
 
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
