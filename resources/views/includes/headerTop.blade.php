@if(Session::get('isloggedin'))
	
	<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
        	<a href="{{ url('logout') }}" class="btn btn-info btn-lg">{{ __('Logout') }}</a>
        	@if(isset($billing_cycle_list))
        	<div class="pull-right">
	        	<div class="dropdown btn-group cycle_dropdown">
				    <a class="btn dropdown-toggle btn btn-info btn-lg" data-toggle="dropdown" href="#">Select Billing Cycle &nbsp; <span class="caret"></span></a>
				    <ul class="dropdown-menu menulist">

				    	@foreach($billing_cycle_list as $item)
				    		{{-- @if(date('Y m d H:i:s',strtotime($item->end_date)) < date(now()) ) --}}
				    		@php( $date_start = strtotime($item->start_date) ) 
							@php( $data_start_conv = date('Y-m-d H:i:s', $date_start) )

							@php( $date_end = strtotime($item->end_date) ) 
							@php( $data_end_conv = date('Y-m-d H:i:s', $date_end) )

							@php( $currentDate = date('Y-m-d H:i:s') )

							@if( $currentDate >= $data_start_conv )

				    			@php ($cycle = date('M. d',strtotime($item->start_date))." - ".date('M. d, Y ',strtotime($item->end_date)))
				    		
						      	<li><a href="{{ url("/getBanCycleLines/{$item->id}") }}">{{ $cycle }}</a></li>

				    		@endif

				    		
					    @endforeach
				    </ul>
				  </div>
	        </div>
			
        	@endif
	    </div>
	</nav>
	

	@if(config('app.goknowsAPI.api_url') == 'https://api-sandbox.goknows.com/v1/')
		<div class="container">
			<div class="alert alert-danger text-center"><strong>Note:</strong> Sandbox Testing API Active</div>
		</div>
	@endif


	@if(isset($targetId) AND !empty($targetId))
		<script>
			$(window).load(function(){
				if($('#{{ $targetId }}')){
				    $('html, body').animate({
					    scrollTop: ($('#{{ $targetId }}').offset().top)
					},500);
				}
			});
		</script>
	@endif
@endif