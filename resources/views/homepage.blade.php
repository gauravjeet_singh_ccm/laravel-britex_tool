<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    GoKnows.com API This is Test Project
                </div>

                {{--  Table For Listing of the Phone Numbers  --}}

                <div class="all_records">
                    
                        @if(isset($result))
                            <table border="1">
                                <tr>
                                    <th>phone_number</th>
                                    <th>sim_number</th>
                                    <th>ban</th>
                                    <th>status</th>
                                </tr>
                                
                                @foreach ($result as $item)

                                    <tr>
                                    <td>{{ $item->phone_number }} </td>
                                    <td>{{ $item->sim_number }} </td>
                                    <td>{{ $item->ban }} </td>
                                    <td>{{ $item->status }} </td>
                                    </tr>

                                @endforeach
                                 

                            </table>

                            {{--  print_r($result);  --}}
                        @endif;
                </div>

                <div class="restore_lines">
                    <h2>Restore lines</h2>
                    <div class="Restore_line_form">
                        
                        <form method="POST" action="{{ url('numberrestorelines') }}">
                            <textarea name="numbers_restore_lines" rows="5" cols="50" placeholder="1 number per line"></textarea>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <p> <input type="submit" name="submit" value="Submit"></p>
                        </form>

                        {{--  <form method="POST" action="{{ url('numberrestorelinestest_put') }}">
                            <textarea name="numbers_restore_lines" rows="5" cols="50" placeholder="1 number per line"></textarea>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p> <input type="submit" name="submit" value="Submit"></p>

                        </form>  --}}

                        <h1 id="title-element"></h1>

                        {{--  <script>
                            var protocol = window.location.protocol;
                            var title = document.getElementById("title-element");

                            if (protocol == 'https:') {
                                title.innerHTML = "You're using HTTPS :)";
                            }else if(protocol == "http:"){
                                title.innerHTML = "This connection isn't secure :( as it uses HTTP";
                            }
                        </script>  --}}

{{--  
                        <hr>

                        <h2>Suspend line</h2>


                        <form action="{{ url('suspendline') }}" method="POST" >
                            <textarea name="numbers_suspend_lines" rows="5" cols="50" placeholder="1 number per line"></textarea>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p> <input type="submit" name="submit" value="Submit"></p>

                        </form>  --}}


                        <hr>

                        <h2>Suspend Bulk lines</h2>


                        <form action="{{ url('suspendBulkline') }}" method="POST" >
                            <textarea name="numbers_suspend_lines" rows="5" cols="50" placeholder="1 number per line"></textarea>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p> <input type="submit" name="submit" value="Submit"></p>

                        </form>


                        <hr>

                        <h2>SIM changes Section</h2>


                        <form action="{{ url('bulk_swap_change_sims') }}" method="POST" enctype="multipart/form-data" >
                            <input type="file" name="csv_upload">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p> <input type="submit" name="submit" value="Submit"></p>

                        </form>


                        <div class="result_swap_sims">
                            @if(isset($result_swap_sims))

                                
                                {{-- {{  "<pre>" }}
                                {{  print_r($result_activate_lines) }}
                                {{ "</pre>" }} --}}
                                
                                <table border="1">
                                    <tr>
                                        <th>Title</th>
                                        <th>Message</th>
                                        <th>Error</th>
                                        <th>Code</th>
                                        <th>Status</th>
                                    </tr>
                                     
                                @foreach ($result_swap_sims as $item)

                                    {{-- {{  print_r($item) }}    --}}

                                    <tr>
                                    <td>{{ $item->title }} </td>
                                    <td>{{ $item->message }} </td>
                                    <td>{{ $item->error }} </td>
                                    <td>{{ $item->code }} </td>
                                    <td>{{ $item->status }} </td>

                                    <tr>

                                @endforeach
                                </table>

                              @endif
                        </div>





                        <hr>

                        <h2>Areacode Changes Section</h2>


                        <form action="{{ url('csv_upload_bulk_areacode_change_sims') }}" method="POST" enctype="multipart/form-data" >
                            <input type="file" name="csv_upload_areacode">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p> <input type="submit" name="submit" value="Submit"></p>

                        </form>


                        <div class="result_areacode_change">
                            @if(isset($result_areacode_change))

                                
                                {{-- {{  "<pre>" }}
                                {{  print_r($result_activate_lines) }}
                                {{ "</pre>" }} --}}
                                
                                <table border="1">
                                    <tr>
                                    
                                        <th>Title</th>
                                        <th>Message</th>
                                        <th>Error</th>
                                        <th>Code</th>
                                        <th>Status</th>
                                    </tr>
                                     
                                @foreach ($result_areacode_change as $item)

                                    {{-- {{  print_r($item) }}    --}}

                                    <tr>
                                    <td>{{ $item->title }} </td>
                                    <td>{{ $item->message }} </td>
                                    <td>{{ $item->error }} </td>
                                    <td>{{ $item->code }} </td>
                                    <td>{{ $item->status }} </td>

                                    <tr>

                                @endforeach
                                </table>

                              @endif
                        </div>



                        <hr>

                        <h2>Bulk Activate Lines Section</h2>


                        <form action="{{ url('bulk_active_line_csv_upload') }}" method="POST" enctype="multipart/form-data" >
                            <input type="file" name="bulk_active_line_csv">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p> <input type="submit" name="submit" value="Submit"></p>

                        </form>


                        <div class="bulk_activate_lines">
                            @if(isset($result_activate_lines))

                                
                                {{-- {{  "<pre>" }}
                                {{  print_r($result_activate_lines) }}
                                {{ "</pre>" }} --}}
                                
                                <table border="1">
                                    <tr>
                                        <th>Title</th>
                                        <th>Message</th>
                                        <th>Error</th>
                                        <th>Code</th>
                                        <th>Status</th>
                                    </tr>
                                     
                                @foreach ($result_activate_lines as $item)

                                    {{-- {{  print_r($item) }} --}}

                                    <tr>
                                    <td>{{ $item->title }} </td>
                                    <td>{{ $item->message }} </td>
                                    <td>{{ $item->error }} </td>
                                    <td>{{ $item->code }} </td>
                                    <td>{{ $item->status }} </td>

                                    <tr>

                                @endforeach
                                </table>

                              @endif
                        </div>


                        <h3>Reports in CSV</h3>

                        <select name="reports_in_csv">
                            <option value="all">All Phone Numbers</option>
                            <option value="specific">Specific Phone Numbers</option>
                            <option value="specific_sims">Specific Sims</option>
                        </select>

                    </div>
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
