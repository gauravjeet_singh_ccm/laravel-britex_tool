@extends('layouts.default')
@section('content')
    <div class='container'>
        <h1 class='title'>Logs</h1>
        <hr>
        @if ($logs)
            <table>
                <tbody>
                    @foreach ($logs as $l)
                    <tr>
                        <td>
                            <span style='font-weight: bold;'>[{{ $l->run_date }}]</span> 
                        </td>
                        <td>
                            <span>{{ $l->error }}</span>
                        </td>
                        <td>
                            @if ($l->success)
                                <span>Cron success</span>
                            @endif
                            <hr>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div style='position: relative; bottom: 0; text-align: right;'>
                {{ $logs->links() }}
            </div>
        @endif
    </div>
@endsection
